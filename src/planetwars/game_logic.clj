(ns planetwars.game-logic
  (:require [cronj.core :as cronj]))

; Description of the game context:
;
; {
; list of existing planets
; :id always > 0
; (
; {:id 1 :x 10 :y 3 :produced 3 :owner 1}
; {:id 2 :x 10 :y 3 :produced 3 :owner 1}
; )
;   :planets '()
;
; list of all teams in the game
; (
; {:id 1 :name \"Team1\"}
; {:id 2 :name \"Team2\"}
; )
;   :teams '()
;
; all bots from all teams. Each bot is agent
; (
; {:id 1 :name \"Player1\" :team-id 1 :state (agent {})}
; {:id 2 :name \"Player2\" :team-id 2 :state (agent {})}
; {:id 3 :name \"Player3\" :team-id 1 :state (agent {})}
; {:id 4 :name \"Player4\" :team-id 2 :state (agent {})}
; )
;   :players '()
;
; list of a messages which should be sent from bots to bots
; (
; {:message 65536 :from 1}
; )
;   :messages '()
;
; list of all separate fleets with it's position (space or planet)
; :arriving-by N ; how many turns until the arrival on the planet (if N > 0 then this fleet on the way)
; if :from-planet-id is equal to :to-planet-id then fleet is located on planet.
; (
; {:owner 1 :from-planet-id 3 :to-planet-id 5 :fleet 4 :arriving-by 3}
; )
;   :fleets '()
;   })

(defn init-game
  "Initialization of the game structures.
  teams-list - list of all teams
  (
    {:id 1 :name \"Team1\"}
    {:id 2 :name \"Team2\"}
  )
  players-list - list of all players
  (
    {:id 1 :name \"Player1\" :team-id 1}
    {:id 2 :name \"Player2\" :team-id 1}
    {:id 3 :name \"Player3\" :team-id 2}
    {:id 4 :name \"Player4\" :team-id 2}
    {:id 5 :name \"Player5\" :team-id 1}
  )"
  [space-size-x
   space-size-y
   num-of-planets
   max-produced
   default-fleet
   teams-list
   players-list]
  (let [context {
                  ; generate teams structure
                  :teams (map #(select-keys % [:id :name]) teams-list)
                  ; generate players structure
                  :players (map #(conj % {:state (agent {})}) players-list)
                  ; generate planets structure
                  :planets (for [id (range 1 (inc num-of-planets))]
                             {:id id
                              :x (rand-int space-size-x) ; TODO: need to check that these coords is unique
                              :y (rand-int space-size-y)
                              :produced (inc (rand-int max-produced)) ; from 1 to max-produced
                              :owner (if
                                       (< (dec id) (count players-list))
                                       (:id ((vec players-list) (dec id)))
                                       0) ; 0 means that it's a neutral planet
                              })
                  ; messages
                  :messages '()
                  }
        ]
    ; generate fleets structure (initially all fleets are based on a planets)
    (conj context {:fleets (map (fn [planetitem]
                                  {:owner (:owner planetitem)
                                   :from-planet-id (:id planetitem)
                                   :to-planet-id (:id planetitem)
                                   :fleet 4
                                   :arriving-by 0})
                             (:planets context))})
    ))

(defn message-to-text
  "Transform bot messsage or world's state message to text message format"
  [msg]
  (let [is-world-snapshot (not (nil? (and (:planets msg) (:message msg) (:id msg))))
        is-order (not (nil? (:from-planet-id msg)))]
    (cond
      ; world snapshot
      is-world-snapshot (let [planets-list (reverse (map
                                                      (fn [planet]
                                                        (str "P " (:id planet) " " (:x planet) " " (:y planet) " " (:produced planet) " " (:owner planet) " " (:fleet planet) "\n"))
                                                      (:planets msg)))
                              result (reverse (conj planets-list (str "M " (:message msg) "\n") (str "Y " (:id msg) "\n")))
                              text (clojure.string/join result)]
                          text)
      is-order (clojure.string/join (list (str "F " (:from-planet-id msg) " " (:to-planet-id msg) " " (:fleet msg) "\n") (str "M " (:message msg) "\n.")))
      :else nil
      )))

(defn text-to-message
  "Transform text message format to bot messsage or world's state message"
  [text-msg]
  (let [str-list (filter #(not (empty? %)) (clojure.string/split text-msg #"\n"))
        planets-list (filter
                       #(not (nil? %))
                       (map
                         (fn [str]
                           (case (get str 0)
                             \P (let [[_ p-id p-x p-y p-produced p-owner p-fleet] (re-find #"P (\d+) (\d+) (\d+) (\d+) (\d+) (\d+)" str)]
                                  {:id (Long/parseLong p-id)
                                   :x (Long/parseLong p-x)
                                   :y (Long/parseLong p-y)
                                   :produced (Long/parseLong p-produced)
                                   :owner (Long/parseLong p-owner)
                                   :fleet (Long/parseLong p-fleet)})
                             nil))
                         str-list))
        message (first (filter
                         #(not (nil? %))
                         (map
                           (fn [str]
                             (case (get str 0)
                               \M (let [[_ m-val] (re-find #"M (\d+)" str)]
                                    (Long/parseLong m-val))
                               nil))
                           str-list)))
        own-id (first (filter
                        #(not (nil? %))
                        (map
                          (fn [str]
                            (case (get str 0)
                              \Y (let [[_ y-val] (re-find #"Y (\d+)" str)]
                                   (Long/parseLong y-val))
                              nil))
                          str-list)))
        move (first (filter
                      #(not (nil? %))
                      (map
                        (fn [str]
                          (case (get str 0)
                            \F (let [[_ f-from f-to f-fleet] (re-find #"F (\d+) (\d+) (\d+)" str)]
                                 {:from-planet-id (Long/parseLong f-from)
                                  :to-planet-id (Long/parseLong f-to)
                                  :fleet (Long/parseLong f-fleet)})
                            nil))
                        str-list)))
        result (if (empty? planets-list)
                 {:from-planet-id (:from-planet-id move)
                  :to-planet-id (:to-planet-id move)
                  :fleet (:fleet move)
                  :message message}
                 {:planets planets-list
                  :id own-id
                  :message message}
                 )
        ]
    result))

(defn battle
  "Calculate a battle.
  The plantes, fleets - list of structures.
  Returns fleets and planets update description"
  [planets fleets]
  (let [new-state (map
                    (fn [planet]
                      (let [defender (first (filter
                                              #(and
                                                 (= (:owner planet) (:owner %))
                                                 (= (:from-planet-id %) (:to-planet-id %) (:id planet)))
                                              fleets))
                            attackers (filter
                                        #(and
                                           (not= (:owner planet) (:owner %))
                                           (= (:from-planet-id %) (:to-planet-id %) (:id planet)))
                                        fleets)
                            ]
                        (cond
                          ; 1 vs 1
                          (= (count attackers) 1) (cond
                                                    (> (:fleet (first attackers)) (:fleet defender))
                                                    (do
                                                      {; attacker won
                                                       :planet-update (list (assoc planet
                                                                              :owner (:owner (first attackers))))
                                                       :fleet-update (list (assoc (first attackers)
                                                                             :fleet (- (:fleet (first attackers)) (:fleet defender))))
                                                       :fleet-remove (list defender)
                                                       })
                                                    (= (:fleet (first attackers)) (:fleet defender))
                                                    (do
                                                      {; defender won, but no fleet
                                                       :fleet-remove (list defender (first attackers))
                                                       })
                                                    (< (:fleet (first attackers)) (:fleet defender))
                                                    (do
                                                      {; defender won
                                                       :fleet-update (list (assoc defender
                                                                             :fleet (- (:fleet defender) (:fleet (first attackers)))))
                                                       :fleet-remove (list (first attackers))
                                                       })
                                                    )
                          ; N vs 1
                          ; more that one attacker
                          (> (count attackers) 1) (let [sorted-attackers (sort
                                                                           (fn [x y] (> (:fleet x) (:fleet y)))
                                                                           attackers)
                                                        strong-attackers (filter #(> (:fleet %) (:fleet defender)) attackers)
                                                        sorted-strong-attackers (sort
                                                                                  (fn [x y] (> (:fleet x) (:fleet y)))
                                                                                  strong-attackers)
                                                        first-strong-attacker (if (> (count sorted-strong-attackers) 0)
                                                                                (first sorted-strong-attackers)
                                                                                nil)
                                                        second-attacker (if (> (count sorted-attackers) 1)
                                                                          (second sorted-attackers)
                                                                          nil)]
                                                    (if (nil? first-strong-attacker)
                                                      {; no strong attackers, so all attackers are lose
                                                       :fleet-remove (conj attackers defender)
                                                       }
                                                      (let [upd-first-strong-attacker (assoc
                                                                                        first-strong-attacker
                                                                                        :fleet (let [resulting-fleet (- (:fleet first-strong-attacker)
                                                                                                                       (:fleet defender)
                                                                                                                       (if (nil? second-attacker)
                                                                                                                         0
                                                                                                                         (:fleet second-attacker)))]
                                                                                                 (if (< resulting-fleet 0) 0 resulting-fleet)))]
                                                        (if (zero? (:fleet upd-first-strong-attacker))
                                                          {; most strong attacker won, but his fleet is zeroed
                                                           :planet-update (list (assoc planet
                                                                                  :owner (:owner first-strong-attacker)))
                                                           :fleet-remove (conj attackers defender)
                                                           }
                                                          {; most strong attacker won
                                                           :planet-update (list (assoc planet
                                                                                  :owner (:owner first-strong-attacker)))
                                                           :fleet-update (list upd-first-strong-attacker)
                                                           :fleet-remove (conj
                                                                           (filter #(not= (:owner %) (:owner first-strong-attacker)) attackers)
                                                                           defender)
                                                           }))))
                          ; no attackers was found so the empty set returns
                          :else {}
                          )
                        ))
                    planets)]
    new-state
    ))

(defn battle-update
  "Updates game context according context-update content, which described needed changes"
  [context context-update]
  (let [updated-planets (let [joined-planet-update-items (first (remove nil? (map #(:planet-update %) context-update)))
                              processed-planets (map
                                                  (fn [planet]
                                                    (let [upd (first (filter #(= (:id planet) (:id %)) joined-planet-update-items))
                                                          res (if (nil? upd) planet (assoc planet :owner (:owner upd)))]
                                                      res))
                                                  (:planets context))
                              ]
                          processed-planets)
        new-fleets-after-remove (let [joined-fleet-remove-items (first (remove nil? (map #(:fleet-remove %) context-update)))
                                      filtered-fleet (remove
                                                       (fn [fleet]
                                                         (some #(= fleet %) joined-fleet-remove-items))
                                                       (:fleets context))]
                                  filtered-fleet)
        new-fleets-after-update (map (fn [fleet]
                                       (first (map (fn [ctx-upd-item]
                                                     (let [upd (first (filter #(= (:owner fleet) (:owner %)) (:fleet-update ctx-upd-item)))
                                                           res (if (nil? upd) fleet (assoc fleet :fleet (:fleet upd)))]
                                                       res))
                                                context-update)))
                                  new-fleets-after-remove)
        updated-ctx (assoc context :planets updated-planets :fleets new-fleets-after-update)
        ]
    updated-ctx
    ))

(defn join-fleets
  "Join fleets with the same owner on all planets.
  Returns new fleets list"
  [planets fleets]
  (let [joined-own-fleet-on-planets (map
                                      (fn [planetitem]
                                        (let [own-fleets-of-this-planet (filter #(and
                                                                                   (= (:owner %) (:owner planetitem))
                                                                                   (= (:from-planet-id %) (:to-planet-id %) (:id planetitem)))
                                                                          fleets)
                                              own-fleets-counts-of-this-planet (map #(:fleet %) own-fleets-of-this-planet)
                                              new-own-fleet-count-on-this-planet (reduce + own-fleets-counts-of-this-planet)
                                              ]
                                          {:owner (:owner planetitem)
                                           :from-planet-id (:id planetitem)
                                           :to-planet-id (:id planetitem)
                                           :fleet new-own-fleet-count-on-this-planet
                                           :arriving-by 0}))
                                      planets)
        ; remove all unjoined fleets from all planets
        filtered-fleet (filter
                         (fn [fleet]
                           (not (some
                                  #(and
                                     (= (:owner fleet) (:owner %))
                                     (= (:from-planet-id fleet) (:to-planet-id fleet) (:from-planet-id %) (:to-planet-id %)))
                                  joined-own-fleet-on-planets)))
                         fleets)
        ; add joined fleets to fleets
        resulted-fleets (concat filtered-fleet joined-own-fleet-on-planets)]
    resulted-fleets))

(defn build-fleets
  "Build new fleet regarding :produced for an each planet.
  Returns new 32 bit value."
  [planets fleets]
  (let [built (map
                (fn [planetitem]
                  {:owner (:owner planetitem)
                   :from-planet-id (:id planetitem)
                   :to-planet-id (:id planetitem)
                   :fleet (:produced planetitem)
                   :arriving-by 0})
                planets)
        new-fleets (concat fleets built)]
    ; after all builds needs to join all fleets on the planets with the same owner.
    (join-fleets planets new-fleets)))

(defn decode-from-32bit-message
  "Encode DWORD into structure.
  If target-planet-id is 0 for a channel then this channel is empty for now."
  [msg-32bit]
  (let [target-planet-id1 (bit-and (bit-shift-right msg-32bit 5) 2r1111111)
        ch1-state (if (zero? target-planet-id1)
                    0
                    {:who-sender (bit-and msg-32bit 2r1)
                     :receiver-id (bit-and (bit-shift-right msg-32bit 1) 2r1111)
                     :target-planet-id target-planet-id1
                     :priority (bit-and (bit-shift-right msg-32bit 12) 2r1)
                     })
        target-planet-id2 (bit-and (bit-shift-right msg-32bit 21) 2r1111111)
        ch2-state (if (zero? target-planet-id2)
                    0
                    {:who-sender (bit-and (bit-shift-right msg-32bit 16) 2r1)
                     :receiver-id (bit-and (bit-shift-right msg-32bit 17) 2r1111)
                     :target-planet-id target-planet-id2
                     :priority (bit-and (bit-shift-right msg-32bit 28) 2r1)
                     })
        ]
    {:ch1 ch1-state :ch2 ch2-state}))

(defn encode-to-32bit-message
  "Encode structure into DWORD and try to put it on first or second channel, when it's free for sending message.
  If target-planet-id is 0 for a channel then this channel is empty for now.
  Returns new 32 bit value."
  [curr-32bit-message-state {who-sender :who-sender
                             receiver-id :receiver-id
                             target-planet-id :target-planet-id
                             priority :priority}]
  (let [{ch1-state :ch1 ch2-state :ch2} (decode-from-32bit-message curr-32bit-message-state)
        new-32bit-message-state (cond
                                  (number? ch1-state) (bit-or
                                                        (bit-and 0xffff0000 curr-32bit-message-state)
                                                        (bit-or
                                                          (bit-shift-left priority 12)
                                                          (bit-shift-left target-planet-id 5)
                                                          (bit-shift-left receiver-id 1)
                                                          who-sender))
                                  (number? ch2-state) (bit-or
                                                        (bit-and 0xffff curr-32bit-message-state)
                                                        (bit-or
                                                          (bit-shift-left priority 28)
                                                          (bit-shift-left target-planet-id 21)
                                                          (bit-shift-left receiver-id 17)
                                                          (bit-shift-left who-sender 16)))
                                  :else :fulfilled)
        ]
    new-32bit-message-state))

(defn calculate-relations-between-two-lists
  "Calculate defined relations between two lists of planets and find most suitable"
  [planet-list1 planet-list2 comparator-fn]
  (let [calculation-list (for [own planet-list1
                               attacked planet-list2
                               :let [x1 (:x own)
                                     x2 (:x attacked)
                                     y1 (:y own)
                                     y2 (:y attacked)]]
                           {:own own
                            :attacked attacked
                            :own-produced (:produced own)
                            :attacked-produced (:produced attacked)
                            :own-fleet (:fleet own) ; current fleet on our planet
                            :attacked-fleet (:fleet own) ; current fleet on attacked planet
                            :produced-diff (- (:produced own) (:produced attacked))
                            :distance (int (+ 0.5 (java.lang.Math/hypot (- x1 x2) (- y1 y2))))})
        ; find most suitable attacked planet related to your planets
        sorted-suitable (sort-by
                          (fn [item] item)
                          comparator-fn
                          calculation-list)
        ; get the planet most suitable for attack on this move
        suitable (first sorted-suitable)]
    suitable))

(defn bot-decision
  "Bot AI function.
  a-val - Clojure agent value
  message - text message described the world's state and mesage from a teammate.
  result of the function - text message described fleet order and message to a next teammate."
  [a-val message]
  ;(time
  (let [msg (text-to-message message)
        ; output message which will be value of the atom
        out-message {}

        ; update msg-state regarding current state
        msg-action (case (:msg-state a-val)
                     ; prepare to send own ID and switch to retranslation mode while own ID won't received
                     nil {:msg-state :id-retranslation ; switch to retranslation state
                          :message (:id msg) ; send own ID
                          :teammates '()}
                     ; remember received ID and retranslate it
                     :id-retranslation (if (= (:id msg) (:message msg)) ; received own ID, so all teammates did received my ID
                                         {:msg-state :send0 ; switch to
                                          :message 0 ; send empty message - signal to end of retranslation mode
                                          :teammates (:teammates a-val)}
                                         {:msg-state :id-retranslation
                                          :message (:message msg) ; need to store this ID and retranslate it to the next teammate
                                          :teammates (conj (:teammates a-val) (:message msg))}
                                         )
                     :send0 (when (= 0 (:message msg)) ; received zero message
                              {:msg-state :op-mode ; switch to operational mode (message exchange between a teammates)
                               :message 0 ; no message for a while, will be filled later if needed
                               :teammates (:teammates a-val)}
                              )
                     )
        ; calculate new a-val
        new-a-val (assoc a-val :msg-state (:msg-state msg-action) :message (:message msg-action) :teammates (:teammates msg-action))

        ; search for attacking a planet
        neutral-planets (filter #(= 0 (:owner %)) (:planets msg))
        ; enemy planets - remove planets with already knowing teammates IDs or my own ID
        ; we don't know IDs of all teammates when (:msg-state new-a-val) is not :op-mode
        enemy-planets (filter
                        (fn [planet]
                          (some #(not= (:owner planet) %) (conj (:teammates new-a-val) (:id msg))))
                        (:planets msg))
        team-planets (filter
                       (fn [planet]
                         (some #(= (:owner planet) %) (conj (:teammates new-a-val) (:id msg))))
                       (:planets msg))
        ; select targets list. First attacks - neutral planets.
        planets-for-attacks (if (empty? neutral-planets)
                              enemy-planets
                              neutral-planets)
        ; calculate all distances between own planets and attacked planets and get the planet most suitable for attack on this move
        own-planets (filter #(= (:id msg) (:owner %)) (:planets msg))
        suitable (calculate-relations-between-two-lists
                   own-planets
                   planets-for-attacks
                   (fn [o1 o2]
                     (cond ; priority of :produced is higher than :distance
                       (< (:produced-diff o1) (:produced-diff o2)) -1
                       (> (:produced-diff o1) (:produced-diff o2)) 1
                       :else (cond
                               (< (:distance o1) (:distance o2)) -1
                               (> (:distance o1) (:distance o2)) 1
                               :else 0)
                       )))

        ; in any case at least one fleet should be available for each our planet

        orders (cond
                 ; already enough fleet for the first wave and enough fleet after produce for the second wave
                 ; so first wave starts now, and the second - on next turn
                 (and
                   (> (:produced-diff suitable) 0)
                   (> (:own-fleet suitable) 1)
                   (> (:own-produced suitable) (:attacked-produced suitable)))
                 {:order {:from-planet-id (:id (:own suitable))
                          :to-planet-id (:id (:attacked suitable))
                          :fleet 1
                          :owner (:id msg)
                          :message (:message new-a-val)}
                  :delayed {:delay 1
                            :order {:from-planet-id (:id (:own suitable))
                                    :to-planet-id (:id (:attacked suitable))
                                    :fleet (inc (:attacked-produced suitable))
                                    :owner (:id msg)
                                    :message (:message new-a-val)}}
                  }
                 ; already enough fleet for the first wave but not enough fleet after produce for the second wave
                 ; so delay first and second waves while fleet for the second wave will be ready
                 (and
                   (> (:produced-diff suitable) 0)
                   (> (:own-fleet suitable) 1)
                   (<= (:own-produced suitable) (:attacked-produced suitable)))
                 {:order {:from-planet-id (:id (:own suitable))
                          :to-planet-id (:id (:own suitable))
                          :fleet 0
                          :owner (:id msg)
                          :message (:message new-a-val)}}

                 ; not enough fleet count for the first wave
                 ; so delay while fleet for the first wave will be ready
                 (and
                   (> (:produced-diff suitable) 0)
                   (<= (:own-fleet suitable) 1))
                 {:order {:from-planet-id (:id (:own suitable))
                          :to-planet-id (:id (:own suitable))
                          :fleet 0
                          :owner (:id msg)
                          :message (:message new-a-val)}}

                 ; our planet produces too small amount of ships
                 ; so need for help from teammates
                 (<= (:produced-diff suitable) 0)
                 (let
                   ; TODO: take into account that the player might no have a planets
                   ; TODO: so in this case it could be a retranslator (should it be removed from teammates list or just marked as retranslator?)
                   [; find teammate id
                    teammate-id (last (:teammates new-a-val))
                    ; find its planets
                    teammate-planets (filter #(= teammate-id (:own %)) team-planets)
                    ; find planet with mostly same distance like the distance from our planet
                    teammate-suitable (calculate-relations-between-two-lists
                                        teammate-planets
                                        (list (:attacked suitable))
                                        (fn [o1 o2]
                                          (cond
                                            (<
                                              (java.lang.Math/abs (- (:distance o1) (:distance suitable)))
                                              (java.lang.Math/abs (- (:distance o2) (:distance suitable)))
                                              ) -1
                                            (>
                                              (java.lang.Math/abs (- (:distance o1) (:distance suitable)))
                                              (java.lang.Math/abs (- (:distance o2) (:distance suitable)))
                                              ) 1
                                            :else 0
                                            )))
                    ; calculate distance diff between my and his planet (need to take into account one turn for sending message)
                    distance-diff (- (inc (:distance teammate-suitable)) (:distance suitable))
                    ; make delay for my fleet or his fleet
                    two-orders (cond
                                 (< distance-diff 0)
                                 ; first wave: need to start my fleet now, and make delay for teammate fleet
                                 {:order {:from-planet-id (:id (:own suitable))
                                          :to-planet-id (:id (:attacked suitable))
                                          :fleet 1
                                          :owner (:id msg)
                                          :message (:message new-a-val)}
                                  :delayed {:delay 1
                                            :order {:from-planet-id (:id (:own teammate-suitable))
                                                    :to-planet-id (:id (:attacked teammate-suitable))
                                                    :fleet 1
                                                    :owner (:owner (:own teammate-suitable))
                                                    :message (:message new-a-val)}}
                                  }
                                 )
                    ; send fleet when it should be
                    ]
                   two-orders)
                 )
        ]
    (assoc out-message :order (message-to-text (:order orders)))
    )) ;)

; TODO: make logging of all actions into console when a game run.

(defn send-messages-to-players-and-wait-for-orders
  "Send message to all players and wait for results.
  planets -
  fleets -
  players - list of all players in the game.
  messages - list of messages, where each message contains current state of all planets, message from teammate, and own ID of the bot, which is receive this message.
    Example: '({:message 65536 :from 0} {:message 65536 :from 2})
  fn-bot - function which implements bot AI.
  For fn-bot functions the response is list which contains order and message to teammate.
  Function returns list of orders from all players."
  [planets fleets teams players messages fn-bot]
  ; send messages to all players async
  (flatten (map
             (fn [team]
               (let [world-planets-snapshot (map
                                              (fn [planet]
                                                (assoc planet
                                                  :fleet (:fleet (first (filter
                                                                          #(and
                                                                             (= (:id planet) (:from-planet-id %) (:to-planet-id %))
                                                                             (= (:owner planet) (:owner %)))
                                                                          fleets)))))
                                              planets)
                     teammates (vec (filter #(= (:id team) (:team-id %)) players)) ; filter by teams
                     cnt (count teammates)
                     op-send-all (for [x (range cnt)]
                                   ; if only one player in the team, he can send the message, and it will be received by him
                                   ; for each message need find a sender and get next player id which is receiver
                                   (let [teammate-from (teammates x)
                                         teammate-to (teammates (if (= x (dec cnt)) 0 (inc x)))
                                         message (first (filter #(= (:from %) (:id teammate-from)) messages))
                                         ; take into account situation when message is nil (it's happens on first move)
                                         world-snapshot (assoc {:planets world-planets-snapshot} :message (if (nil? message) 0 (:message message)) :id (:id teammate-to))
                                         text-msg (message-to-text world-snapshot)]
                                     (send (:state teammate-to) fn-bot text-msg)))
                     bool-results (do
                                    (dorun op-send-all)
                                    ; wait for 1000ms maximum for each player
                                    ; TODO: need to process timeout
                                    (map #(keyword (str (await-for 1000 (:state %)))) teammates))
                     ; get all new states w/o timeouts
                     results (filter
                               #(= (val %) :true)
                               (apply assoc {} (interleave teammates bool-results)))
                     ; create orders like this {:from-planet-id 1 :to-planet-id 2 :fleet 1 :owner 0 :message 65536}
                     orders (map
                              (fn [result]
                                (let [teammate (key result)
                                      text-response (:order @(:state teammate))
                                      response (text-to-message text-response)
                                      order (assoc response :owner (:id teammate))]
                                  order))
                              results)
                     ]
                 orders))
             teams)
    ))

(defn apply-orders
  "Applying all orders. The function implements a fleets move for an each player's order.
  Also move all fleets which are already on the way."
  [orders fleets planets]
  (let [fleets-ready-to-flight (map (fn [order]
                                      (let [{ofrom :from-planet-id
                                             oto :to-planet-id
                                             ofleet-count :fleet
                                             oowner :owner
                                             omsg :message} order ; destructuring
                                            {fowner :owner
                                             ffrom :from-planet-id
                                             fto :to-planet-id
                                             ffleet-count :fleet
                                             farriving :arriving-by} (first (filter (fn [fleet-item] (and
                                                                                                       (= (:owner fleet-item) oowner)
                                                                                                       (= (:from-planet-id fleet-item) ofrom)
                                                                                                       (= (:to-planet-id fleet-item) ofrom)))
                                                                              fleets)) ; destructuring
                                            distance-between-planets (let [[{id1 :id x1 :x y1 :y produced1 :produced owner1 :owner}
                                                                            {id2 :id x2 :x y2 :y produced2 :produced owner2 :owner}]
                                                                           (filter #(or (= (:id %) ofrom) (= (:id %) oto)) planets)]
                                                                       (int (+ 0.5 (java.lang.Math/hypot (- x1 x2) (- y1 y2))))) ; destructuring
                                            prepare-fleet-for-fly {
                                                                    :orig {:owner fowner
                                                                           :from-planet-id ffrom
                                                                           :to-planet-id fto
                                                                           :fleet ffleet-count
                                                                           :arriving-by farriving} ; orig fleet
                                                                    :new (list
                                                                           {:owner fowner
                                                                            :from-planet-id ffrom
                                                                            :to-planet-id ffrom
                                                                            :fleet (- ffleet-count ofleet-count)
                                                                            :arriving-by 0} ; stay
                                                                           {:owner fowner
                                                                            :from-planet-id ofrom
                                                                            :to-planet-id oto
                                                                            :fleet ofleet-count
                                                                            :arriving-by distance-between-planets})} ; on the planet + ready to fly
                                            ]
                                        prepare-fleet-for-fly
                                        ))
                                 orders)
        new-fleets-unflatten (map
                               (fn [fleet]
                                 (let [found (filter #(= fleet (:orig %)) fleets-ready-to-flight)
                                       replaced (if (empty? found)
                                                  fleet ; replace orig fleet by new two fleets
                                                  (:new (first found))) ; orig fleet unchaged
                                       ]
                                   replaced))
                               fleets)
        new-fleets (flatten new-fleets-unflatten)
        ; move fleets which are already on the way
        fleets-after-the-move (map
                                (fn [fleet]
                                  (if (> (:arriving-by fleet) 0)
                                    ; fleet on the way - make next move
                                    (let [moves-left (:arriving-by fleet)
                                          new-moves-left (dec moves-left)]
                                      (if (zero? new-moves-left)
                                        ; fleet arrived to planet
                                        (assoc fleet :from-planet-id (:to-planet-id fleet) :arriving-by 0)
                                        ; fleet still on the way
                                        (assoc fleet :arriving-by new-moves-left)))
                                    ; fleet on planet
                                    fleet))
                                new-fleets)
        prepared-messages (map
                            ; 'from' field was introduced find the next teammate for sending the message to him
                            ; 'from' field won't taken into account when text message created for a bot
                            #(identity {:message (:message %) :from (:owner %)})
                            orders)]
    ; after all moves we need to join all fleets on the planets with the same owner, before a battle begins
    {:fleets (join-fleets planets fleets-after-the-move) :messages prepared-messages})
  )

(defn next-move
  "Calculate next game move.
  context - current game context
  fn-player - bot AI function"
  [context fn-player]
  (let [ctx1-update (battle (:planets context) (:fleets context)) ; battles on the planets
        ctx1 (battle-update context ctx1-update)
        ; build additional fleets on a planets
        ctx2 (assoc ctx1 :fleets (build-fleets (:planets ctx1) (:fleets ctx1)))
        ; send message to all bots asynchronously and wait for decisions from all bots
        orders (send-messages-to-players-and-wait-for-orders
                 (:planets ctx2) (:fleets ctx2) (:teams ctx2) (:players ctx2) (:messages ctx2) fn-player)
        ; apply all orders from all players
        fleets-and-messages (apply-orders orders (:fleets ctx2) (:planets ctx2))
        ctx3 (assoc ctx2 :fleets (:fleets fleets-and-messages) :messages (:messages fleets-and-messages))
        ]
    ctx3))
