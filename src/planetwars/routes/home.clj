(ns planetwars.routes.home
  (:require [planetwars.layout :as layout]
            [planetwars.util :as util]
            [planetwars.game-logic :as logic]
            [compojure.core :refer :all]
            [noir.response :refer [edn]]
            [clojure.pprint :refer [pprint]]))

(def game-context (atom {}))

(defn home-page []
  (layout/render
    "app.html" {:docs (util/md->html "/md/docs.md")}))

(defn parse-int [s]
  (Integer. (re-find #"\d+" s)))

(defn save-document [doc]
  (pprint doc)
  {:status "ok"})

(defroutes home-routes
  (GET "/" [] (home-page))

  ; "init game" action
  (POST "/init" [] (fn [req]
                     (let [{
                             space-size-x :space-size-x
                             space-size-y :space-size-y
                             num-of-planets :num-of-planets
                             max-produced :max-produced
                             default-fleet :default-fleet
                             num-of-teams :num-of-teams
                             players-list :players-list
                             } (:params req)
                           teams-list (for [n (range 1 (inc (parse-int num-of-teams)))] {:id n :name (str "Team" n)})
                           context (logic/init-game
                                     (parse-int space-size-x)
                                     (parse-int space-size-y)
                                     (parse-int num-of-planets)
                                     (parse-int max-produced)
                                     (parse-int default-fleet)
                                     teams-list
                                     (map
                                       #(let [name (name (key %))
                                              id (parse-int name)
                                              value (val %)
                                              team-id (parse-int value)]
                                          {:id id :name name :team-id team-id})
                                       players-list)
                                     )]
                       (reset! game-context context)
                       {:status 200})))

  ; get current game context
  (GET "/context" [] (let [players (:players @game-context)
                           new-players (map #(dissoc % :state) players)
                           new-context (assoc @game-context :players new-players)
                           ]
                       (edn new-context)))
  ; "next move" action
  (GET "/next" [] (do
                    (when (not (empty? @game-context))
                      (swap! game-context logic/next-move logic/bot-decision)
                      {:status 200})))

  (POST "/save" {:keys [body-params]}
    (edn (save-document body-params))))
