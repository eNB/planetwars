(ns planetwars.test.game-logic
  (:use [clojure.test]
        [planetwars.game-logic :as l])
  (:import (java.lang.Math/sqrt)))

; TODO: also check for bad params

; TODO: format all code

(deftest test-app
  (testing "init-game."
    (let [space-size-x 10
          space-size-y 10
          num-of-planets 10
          max-produced 5
          default-fleet 3
          teams-list '(
                        {:id 1 :name "Team1"}
                        {:id 2 :name "Team2"}
                        {:id 3 :name "Team3"}
                        )
          players-list '(
                          {:id 1 :name "Player1" :team-id 1}
                          {:id 2 :name "Player2" :team-id 1}
                          {:id 3 :name "Player3" :team-id 1}

                          {:id 4 :name "Player4" :team-id 2}
                          {:id 5 :name "Player5" :team-id 2}

                          {:id 6 :name "Player6" :team-id 3}
                          {:id 7 :name "Player7" :team-id 3}
                          {:id 8 :name "Player8" :team-id 3}
                          )
          context (l/init-game
                    space-size-x
                    space-size-y
                    num-of-planets
                    max-produced
                    default-fleet
                    teams-list
                    players-list)]

      ; test of teams structure
      (is (=
            '(
               {:id 1 :name "Team1"}
               {:id 2 :name "Team2"}
               {:id 3 :name "Team3"}
               )
            (:teams context)))
      ; test of players structure
      (is (=
            '(
               {:id 1 :name "Player1" :team-id 1}
               {:id 2 :name "Player2" :team-id 1}
               {:id 3 :name "Player3" :team-id 1}

               {:id 4 :name "Player4" :team-id 2}
               {:id 5 :name "Player5" :team-id 2}

               {:id 6 :name "Player6" :team-id 3}
               {:id 7 :name "Player7" :team-id 3}
               {:id 8 :name "Player8" :team-id 3}
               )
            (map #(dissoc % :state) (:players context)) ; remove :state kv because it's not important to compare
            ))
      ; test of planets structure
      ; TODO: improve this test
      (is (= 10 (count (:planets context))))
      (is (some #(> (:produced %) 0) (:planets context)))
      (is (not (nil? (:id (first (:planets context))))))
      (is (not (nil? (:x (first (:planets context))))))
      (is (not (nil? (:y (first (:planets context))))))
      (is (< (:produced (first (:planets context))) (inc max-produced)))
      (is (not (nil? (:owner (first (:planets context))))))
      ;test of fleets structure
      (is (= 10 (count (:fleets context))))
      (is (not (nil? (:owner (first (:fleets context))))))
      (is (not (nil? (:from-planet-id (first (:fleets context))))))
      (is (not (nil? (:to-planet-id (first (:fleets context))))))
      (is (not (nil? (:fleet (first (:fleets context))))))
      (is (not (nil? (:arriving-by (first (:fleets context))))))
      ))

  (testing "text-to-message and message-to-text."
    (let [textmsg1 "P 1 2 3 1 0 1\nP 3 0 0 2 1 1\nP 2 3 4 2 2 1\nM 236746\nY 1\n"
          textmsg2 "F 3 1 1\nM 23777\n."
          msg1 {
                 :planets '(
                             {:id 1 :x 2 :y 3 :produced 1 :owner 0 :fleet 1}
                             {:id 3 :x 0 :y 0 :produced 2 :owner 1 :fleet 1}
                             {:id 2 :x 3 :y 4 :produced 2 :owner 2 :fleet 1}
                             )
                 :message 236746
                 :id 1
                 }
          msg2 {:from-planet-id 3 :to-planet-id 1 :fleet 1 :message 23777} ; {:owner 1} added later when it will be known
          ]
      (is (= textmsg1 (l/message-to-text msg1)))
      (is (= textmsg2 (l/message-to-text msg2)))
      (is (= msg1 (l/text-to-message textmsg1)))
      (is (= msg2 (l/text-to-message textmsg2)))
      ))

  (testing "decode-from-32bit-message and encode-to-32bit-message."
    (let [data1 {:who-sender 1
                 :receiver-id 10
                 :target-planet-id 100
                 :priority 1}
          data2 {:who-sender 0
                 :receiver-id 9
                 :target-planet-id 99
                 :priority 0}
          expected {:ch1 data1 :ch2 data2}
          msg1 (encode-to-32bit-message 0 data1)
          msg2 (encode-to-32bit-message msg1 data2)
          ]
      (is (= (l/decode-from-32bit-message msg2) expected))))

  (testing "battle #1. 1 vs 1. Attacker won."
    (let [planets '(; neutral planet
                     {:id 1 :x 3 :y 2 :produced 3 :owner 0}
                     )
          fleets '(
                    {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0}
                    {:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 5 :arriving-by 0}
                    )
          expected-update '({
                              :planet-update ({:id 1 :x 3 :y 2 :produced 3 :owner 1})
                              :fleet-update ({:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 2 :arriving-by 0})
                              :fleet-remove ({:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0})
                              })
          update (l/battle planets fleets)]
      (is (=
            expected-update
            update))
      ))
  (testing "battle #2. 1 vs 1. Defender won."
    (let [planets '(; neutral planet
                     {:id 1 :x 3 :y 2 :produced 3 :owner 0}
                     )
          fleets '(
                    {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0}
                    {:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0}
                    )
          expected-update '({
                              :fleet-remove (
                                              {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0}
                                              {:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0})
                              })
          update (l/battle planets fleets)]
      (is (=
            expected-update
            update))
      ))
  (testing "battle #3. 1 vs 1. Defender won."
    (let [planets '(; neutral planet
                     {:id 1 :x 3 :y 2 :produced 3 :owner 0}
                     )
          fleets '(
                    {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 5 :arriving-by 0}
                    {:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0}
                    )
          expected-update '({
                              :fleet-update ({:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 2 :arriving-by 0})
                              :fleet-remove ({:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0})
                              })
          update (l/battle planets fleets)]
      (is (=
            expected-update
            update))
      ))
  (testing "battle #4. N vs 1. Defender won."
    (let [planets '(; neutral planet
                     {:id 1 :x 3 :y 2 :produced 3 :owner 0}
                     )
          fleets '(
                    {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 5 :arriving-by 0}
                    {:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0}
                    {:owner 2 :from-planet-id 1 :to-planet-id 1 :fleet 2 :arriving-by 0}
                    {:owner 3 :from-planet-id 1 :to-planet-id 1 :fleet 1 :arriving-by 0}
                    )
          expected-update '({
                              :fleet-remove (
                                              {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 5 :arriving-by 0}
                                              {:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0}
                                              {:owner 2 :from-planet-id 1 :to-planet-id 1 :fleet 2 :arriving-by 0}
                                              {:owner 3 :from-planet-id 1 :to-planet-id 1 :fleet 1 :arriving-by 0})
                              })
          update (l/battle planets fleets)]
      (is (=
            expected-update
            update))
      ))
  (testing "battle #5. N vs 1. One of the attackers won."
    (let [planets '(; neutral planet
                     {:id 1 :x 3 :y 2 :produced 3 :owner 0}
                     )
          fleets '(
                    {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 5 :arriving-by 0}
                    {:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0}
                    {:owner 2 :from-planet-id 1 :to-planet-id 1 :fleet 2 :arriving-by 0}
                    {:owner 3 :from-planet-id 1 :to-planet-id 1 :fleet 10 :arriving-by 0}
                    )
          expected-update '({
                              :planet-update ({:id 1 :x 3 :y 2 :produced 3 :owner 3})
                              :fleet-update ({:owner 3 :from-planet-id 1 :to-planet-id 1 :fleet 2 :arriving-by 0})
                              :fleet-remove (
                                              {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 5 :arriving-by 0}
                                              {:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0}
                                              {:owner 2 :from-planet-id 1 :to-planet-id 1 :fleet 2 :arriving-by 0})
                              })
          update (l/battle planets fleets)]
      (is (=
            expected-update
            update))
      ))
  (testing "battle #6. N vs 1. One of the attackers won but his fleet is zeroed."
    (let [planets '(; neutral planet
                     {:id 1 :x 3 :y 2 :produced 3 :owner 0}
                     )
          fleets '(
                    {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 5 :arriving-by 0}
                    {:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0}
                    {:owner 2 :from-planet-id 1 :to-planet-id 1 :fleet 2 :arriving-by 0}
                    {:owner 3 :from-planet-id 1 :to-planet-id 1 :fleet 6 :arriving-by 0}
                    )
          expected-update '({
                              :planet-update ({:id 1 :x 3 :y 2 :produced 3 :owner 3})
                              :fleet-remove (
                                              {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 5 :arriving-by 0}
                                              {:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0}
                                              {:owner 2 :from-planet-id 1 :to-planet-id 1 :fleet 2 :arriving-by 0}
                                              {:owner 3 :from-planet-id 1 :to-planet-id 1 :fleet 6 :arriving-by 0})
                              })
          update (l/battle planets fleets)]
      (is (=
            expected-update
            update))
      ))
  (testing "battle #7. N vs 1. One of the attackers won (doesn't matter who is this) but his fleet is zeroed."
    (let [planets '(; neutral planet
                     {:id 1 :x 3 :y 2 :produced 3 :owner 0}
                     )
          fleets '(
                    {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 5 :arriving-by 0}
                    {:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0}
                    {:owner 2 :from-planet-id 1 :to-planet-id 1 :fleet 10 :arriving-by 0} ; winner
                    {:owner 3 :from-planet-id 1 :to-planet-id 1 :fleet 10 :arriving-by 0} ; could be winner but will not
                    )
          expected-update '({
                              :planet-update ({:id 1 :x 3 :y 2 :produced 3 :owner 2})
                              :fleet-remove (
                                              {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 5 :arriving-by 0}
                                              {:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0}
                                              {:owner 2 :from-planet-id 1 :to-planet-id 1 :fleet 10 :arriving-by 0}
                                              {:owner 3 :from-planet-id 1 :to-planet-id 1 :fleet 10 :arriving-by 0})
                              })
          update (l/battle planets fleets)]
      (is (=
            expected-update
            update))
      ))
  (testing "battle #8. N vs 1. One of the attackers won (doesn't matter who is this) but his fleet is zeroed."
    (let [planets '(; neutral planet
                     {:id 1 :x 3 :y 2 :produced 3 :owner 0}
                     )
          fleets '(
                    {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 9 :arriving-by 0}
                    {:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 10 :arriving-by 0} ; winner
                    {:owner 2 :from-planet-id 1 :to-planet-id 1 :fleet 10 :arriving-by 0} ; could be winner but will not
                    {:owner 3 :from-planet-id 1 :to-planet-id 1 :fleet 10 :arriving-by 0} ; could be winner but will not
                    )
          expected-update '({
                              :planet-update ({:id 1 :x 3 :y 2 :produced 3 :owner 1})
                              :fleet-remove (
                                              {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 9 :arriving-by 0}
                                              {:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 10 :arriving-by 0}
                                              {:owner 2 :from-planet-id 1 :to-planet-id 1 :fleet 10 :arriving-by 0}
                                              {:owner 3 :from-planet-id 1 :to-planet-id 1 :fleet 10 :arriving-by 0})
                              })
          update (l/battle planets fleets)]
      (is (=
            expected-update
            update))
      ))

  (testing "battle-update #1"
    (let [context {:planets '(
                               {:id 1 :x 3 :y 2 :produced 3 :owner 0}
                               {:id 2 :x 4 :y 5 :produced 5 :owner 2}
                               {:id 3 :x 6 :y 7 :produced 8 :owner 3}
                               )
                   :fleets '({:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 5 :arriving-by 0}
                              {:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0}
                              {:owner 2 :from-planet-id 1 :to-planet-id 1 :fleet 10 :arriving-by 0}
                              {:owner 3 :from-planet-id 1 :to-planet-id 1 :fleet 10 :arriving-by 0})}
          context-update '({
                             :planet-update ({:id 2 :x 3 :y 2 :produced 5 :owner 3}) ; TODO: :planet-update {:id 2 :x 3 :y 2 :produced 5 :owner 3} should be
                             :fleet-update ({:owner 3 :from-planet-id 1 :to-planet-id 1 :fleet 1 :arriving-by 0})
                             :fleet-remove ({:owner 2 :from-planet-id 1 :to-planet-id 1 :fleet 10 :arriving-by 0})
                             })
          expected-context {:planets '(
                                        {:id 1 :x 3 :y 2 :produced 3 :owner 0}
                                        {:id 2 :x 4 :y 5 :produced 5 :owner 3}
                                        {:id 3 :x 6 :y 7 :produced 8 :owner 3}
                                        )
                            :fleets '({:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 5 :arriving-by 0}
                                       {:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0}
                                       {:owner 3 :from-planet-id 1 :to-planet-id 1 :fleet 1 :arriving-by 0})}]
      (is (= expected-context (l/battle-update context context-update)))))
  (testing "battle-update #2"
    (let [context {:planets '(
                               {:id 1 :x 3 :y 2 :produced 3 :owner 0}
                               {:id 2 :x 4 :y 5 :produced 5 :owner 2}
                               {:id 3 :x 6 :y 7 :produced 8 :owner 3}
                               )
                   :fleets '({:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 5 :arriving-by 0}
                              {:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0}
                              {:owner 2 :from-planet-id 1 :to-planet-id 1 :fleet 10 :arriving-by 0}
                              {:owner 3 :from-planet-id 1 :to-planet-id 1 :fleet 10 :arriving-by 0})}
          context-update '({
                             :fleet-remove ({:owner 2 :from-planet-id 1 :to-planet-id 1 :fleet 10 :arriving-by 0})
                             })
          expected-context {:planets '(
                                        {:id 1 :x 3 :y 2 :produced 3 :owner 0}
                                        {:id 2 :x 4 :y 5 :produced 5 :owner 2}
                                        {:id 3 :x 6 :y 7 :produced 8 :owner 3}
                                        )
                            :fleets '({:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 5 :arriving-by 0}
                                       {:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0}
                                       {:owner 3 :from-planet-id 1 :to-planet-id 1 :fleet 10 :arriving-by 0})}]
      (is (= expected-context (l/battle-update context context-update)))))
  (testing "battle-update #3"
    (let [context {:planets '(
                               {:id 1 :x 3 :y 2 :produced 3 :owner 0}
                               {:id 2 :x 4 :y 5 :produced 5 :owner 2}
                               {:id 3 :x 6 :y 7 :produced 8 :owner 3}
                               )
                   :fleets '({:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 5 :arriving-by 0}
                              {:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0}
                              {:owner 2 :from-planet-id 1 :to-planet-id 1 :fleet 10 :arriving-by 0}
                              {:owner 3 :from-planet-id 1 :to-planet-id 1 :fleet 10 :arriving-by 0})}
          context-update '({
                             :planet-update ({:id 2 :x 3 :y 2 :produced 5 :owner 3}) ; TODO: :planet-update {:id 2 :x 3 :y 2 :produced 5 :owner 3} should be
                             :fleet-remove ({:owner 2 :from-planet-id 1 :to-planet-id 1 :fleet 10 :arriving-by 0})
                             })
          expected-context {:planets '(
                                        {:id 1 :x 3 :y 2 :produced 3 :owner 0}
                                        {:id 2 :x 4 :y 5 :produced 5 :owner 3}
                                        {:id 3 :x 6 :y 7 :produced 8 :owner 3}
                                        )
                            :fleets '({:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 5 :arriving-by 0}
                                       {:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0}
                                       {:owner 3 :from-planet-id 1 :to-planet-id 1 :fleet 10 :arriving-by 0})}]
      (is (= expected-context (l/battle-update context context-update)))))

  (testing "build-fleets #1 All planets has a fleet"
    (let [fleets '(
                    {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 5 :arriving-by 0}
                    {:owner 1 :from-planet-id 2 :to-planet-id 2 :fleet 3 :arriving-by 0}
                    {:owner 2 :from-planet-id 3 :to-planet-id 3 :fleet 10 :arriving-by 0}
                    )
          planets '(
                     {:id 1 :x 3 :y 2 :produced 3 :owner 0}
                     {:id 2 :x 4 :y 5 :produced 5 :owner 1}
                     {:id 3 :x 6 :y 7 :produced 8 :owner 2}
                     )
          expected-fleets '(
                             {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 8 :arriving-by 0}
                             {:owner 1 :from-planet-id 2 :to-planet-id 2 :fleet 8 :arriving-by 0}
                             {:owner 2 :from-planet-id 3 :to-planet-id 3 :fleet 18 :arriving-by 0}
                             )]
      (is (= expected-fleets (build-fleets planets fleets)))))
  (testing "build-fleets #2 No planets has a fleet"
    (let [fleets '()
          planets '(
                     {:id 1 :x 3 :y 2 :produced 3 :owner 0}
                     {:id 2 :x 4 :y 5 :produced 5 :owner 1}
                     {:id 3 :x 6 :y 7 :produced 8 :owner 2}
                     )
          expected-fleets '(
                             {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0}
                             {:owner 1 :from-planet-id 2 :to-planet-id 2 :fleet 5 :arriving-by 0}
                             {:owner 2 :from-planet-id 3 :to-planet-id 3 :fleet 8 :arriving-by 0}
                             )]
      (is (= expected-fleets (build-fleets planets fleets)))))
  (testing "build-fleets #3 Only one planet has a fleet"
    (let [fleets '(
                    {:owner 1 :from-planet-id 2 :to-planet-id 2 :fleet 3 :arriving-by 0}
                    )
          planets '(
                     {:id 1 :x 3 :y 2 :produced 3 :owner 0}
                     {:id 2 :x 4 :y 5 :produced 5 :owner 1}
                     {:id 3 :x 6 :y 7 :produced 8 :owner 2}
                     )
          expected-fleets '(
                             {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 3 :arriving-by 0}
                             {:owner 1 :from-planet-id 2 :to-planet-id 2 :fleet 8 :arriving-by 0}
                             {:owner 2 :from-planet-id 3 :to-planet-id 3 :fleet 8 :arriving-by 0}
                             )]
      (is (= expected-fleets (build-fleets planets fleets)))))
  (testing "build-fleets #4 All planets has a fleet, and more than one neutral planet."
    (let [fleets '(
                    {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 5 :arriving-by 0}
                    {:owner 0 :from-planet-id 4 :to-planet-id 4 :fleet 5 :arriving-by 0}
                    {:owner 1 :from-planet-id 2 :to-planet-id 2 :fleet 3 :arriving-by 0}
                    {:owner 2 :from-planet-id 3 :to-planet-id 3 :fleet 10 :arriving-by 0}
                    )
          planets '(
                     {:id 1 :x 3 :y 2 :produced 3 :owner 0}
                     {:id 2 :x 4 :y 5 :produced 5 :owner 1}
                     {:id 3 :x 6 :y 7 :produced 8 :owner 2}
                     {:id 4 :x 8 :y 1 :produced 5 :owner 0}
                     )
          expected-fleets '(
                             {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 8 :arriving-by 0}
                             {:owner 1 :from-planet-id 2 :to-planet-id 2 :fleet 8 :arriving-by 0}
                             {:owner 2 :from-planet-id 3 :to-planet-id 3 :fleet 18 :arriving-by 0}
                             {:owner 0 :from-planet-id 4 :to-planet-id 4 :fleet 10 :arriving-by 0}
                             )]
      (is (= expected-fleets (build-fleets planets fleets)))))

  (testing "apply-orders #1"
    (let [orders '(
                    {:from-planet-id 3 :to-planet-id 1 :fleet 4 :owner 2 :message 65536}
                    )
          fleets '(
                    {:owner 1 :from-planet-id 2 :to-planet-id 1 :fleet 1 :arriving-by 2} ; fleet on the way
                    {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 5 :arriving-by 0}
                    {:owner 1 :from-planet-id 2 :to-planet-id 2 :fleet 3 :arriving-by 0}
                    {:owner 2 :from-planet-id 3 :to-planet-id 3 :fleet 10 :arriving-by 0}
                    )
          planets '(
                     {:id 1 :x 1 :y 2 :produced 3 :owner 0}
                     {:id 2 :x 2 :y 2 :produced 5 :owner 1}
                     {:id 3 :x 6 :y 7 :produced 8 :owner 2}
                     )
          expected-fleets '(
                             {:owner 1 :from-planet-id 2 :to-planet-id 1 :fleet 1 :arriving-by 1} ; fleet is still on the way
                             {:owner 2 :from-planet-id 3 :to-planet-id 1 :fleet 4 :arriving-by 6}
                             {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 5 :arriving-by 0}
                             {:owner 1 :from-planet-id 2 :to-planet-id 2 :fleet 3 :arriving-by 0}
                             {:owner 2 :from-planet-id 3 :to-planet-id 3 :fleet 6 :arriving-by 0}
                             )
          expected-messages '(
                               {:message 65536 :from 2}
                               )
          expected-struct {:fleets expected-fleets :messages expected-messages}]
      (is (= expected-struct (apply-orders orders fleets planets)))))

  ; simple bot AI function
  (defn fn-bot1
    "a - agent
    message - text message"
    [a message]
    (let [msg (text-to-message message)
          ; search for all neutral planets
          neutral-planets (filter #(= 0 (:owner %)) (:planets msg))
          own-planets (filter #(= (:id msg) (:owner %)) (:planets msg))
          ; calculate all distances between own planets and neutral planets
          calculated-dist (for [own own-planets
                                neutral neutral-planets
                                :let [x1 (:x own)
                                      x2 (:x neutral)
                                      y1 (:y own)
                                      y2 (:y neutral)]]
                            {:own own :neutral neutral :distance (int (+ 0.5 (java.lang.Math/hypot (- x1 x2) (- y1 y2))))})
          ; find nearest neutral planet related to your planets
          sorted-dist (sort-by #(:distance %) < calculated-dist)
          ; find nearest own fleet relative to the planet
          nearest (first sorted-dist)
          ; generate order: send all fleet (most simple solution for testing) to the planet
          order {:from-planet-id (:id (:own nearest))
                 :to-planet-id (:id (:neutral nearest))
                 :fleet (:fleet (:own nearest))
                 :owner (:id msg)
                 :message 0xdeafbeef}]
      {
        :order (message-to-text order)
        }
      ))
  (testing "send-messages-to-players-and-wait-for-orders #1. 1 vs 1"
    (let [planets '(
                     {:id 1 :x 0 :y 0 :produced 2 :owner 1}
                     {:id 3 :x 7 :y 0 :produced 2 :owner 2}
                     {:id 2 :x 0 :y 6 :produced 2 :owner 0}
                     {:id 4 :x 7 :y 6 :produced 2 :owner 0}
                     {:id 5 :x 2 :y 3 :produced 2 :owner 0}
                     {:id 6 :x 5 :y 3 :produced 2 :owner 0}
                     )
          fleets '(
                    {:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 4 :arriving-by 0}
                    {:owner 2 :from-planet-id 3 :to-planet-id 3 :fleet 5 :arriving-by 0}
                    {:owner 0 :from-planet-id 2 :to-planet-id 2 :fleet 3 :arriving-by 0}
                    {:owner 0 :from-planet-id 4 :to-planet-id 4 :fleet 6 :arriving-by 0}
                    {:owner 0 :from-planet-id 5 :to-planet-id 5 :fleet 8 :arriving-by 0}
                    {:owner 0 :from-planet-id 6 :to-planet-id 6 :fleet 4 :arriving-by 0}
                    )
          teams '(
                   {:id 1 :name "Team1"}
                   {:id 2 :name "Team2"}
                   )
          players (list
                    {:id 1 :name "Player1" :team-id 1 :state (agent {})}
                    {:id 2 :name "Player2" :team-id 2 :state (agent {})}
                    )
          messages '(
                      {:message 0xdeafbeef :from 1}
                      {:message 0xdeafbeef :from 2}
                      )
          orders '(
                    {:from-planet-id 1 :to-planet-id 5 :fleet 4 :owner 1 :message 0xdeafbeef}
                    {:from-planet-id 3 :to-planet-id 6 :fleet 5 :owner 2 :message 0xdeafbeef}
                    )
          ]
      (is (= orders (send-messages-to-players-and-wait-for-orders planets fleets teams players messages fn-bot1)))))

  ; simple bot AI function
  (defn fn-bot2
    "a - agent
    message - text message"
    [a message]
    (let [msg (text-to-message message)
          ; search for all neutral planets
          neutral-planets (filter #(= 0 (:owner %)) (:planets msg))
          own-planets (filter #(= (:id msg) (:owner %)) (:planets msg))
          ; calculate all distances between own planets and neutral planets
          calculated-dist (for [own own-planets
                                neutral neutral-planets
                                :let [x1 (:x own)
                                      x2 (:x neutral)
                                      y1 (:y own)
                                      y2 (:y neutral)]]
                            {:own own :neutral neutral :distance (int (+ 0.5 (java.lang.Math/hypot (- x1 x2) (- y1 y2))))})
          ; find nearest neutral planet related to your planets
          sorted-dist (sort-by #(:distance %) < calculated-dist)
          ; find nearest own fleet relative to the planet
          nearest (first sorted-dist)
          ; generate order: send all fleet (most simple solution for testing) to the planet
          order {:from-planet-id (:id (:own nearest))
                 :to-planet-id (:id (:neutral nearest))
                 :fleet (:fleet (:own nearest))
                 :owner (:id msg)
                 :message 0xdeafbeef}]
      {
        :order (message-to-text order)
        }
      ))
  (testing "next-move"
    (let [planets '(
                     {:id 1 :x 0 :y 0 :produced 2 :owner 1}
                     {:id 3 :x 7 :y 0 :produced 2 :owner 2}
                     {:id 2 :x 0 :y 6 :produced 2 :owner 0}
                     {:id 4 :x 7 :y 6 :produced 2 :owner 0}
                     {:id 5 :x 2 :y 3 :produced 2 :owner 0}
                     {:id 6 :x 5 :y 3 :produced 2 :owner 0}
                     )
          fleets '(
                    {:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 4 :arriving-by 0}
                    {:owner 2 :from-planet-id 3 :to-planet-id 3 :fleet 5 :arriving-by 0}
                    {:owner 0 :from-planet-id 2 :to-planet-id 2 :fleet 3 :arriving-by 0}
                    {:owner 0 :from-planet-id 4 :to-planet-id 4 :fleet 6 :arriving-by 0}
                    {:owner 0 :from-planet-id 5 :to-planet-id 5 :fleet 8 :arriving-by 0}
                    {:owner 0 :from-planet-id 6 :to-planet-id 6 :fleet 4 :arriving-by 0}
                    )
          teams '(
                   {:id 1 :name "Team1"}
                   {:id 2 :name "Team2"}
                   )
          players (list
                    {:id 1 :name "Player1" :team-id 1 :state (agent {})}
                    {:id 2 :name "Player2" :team-id 2 :state (agent {})}
                    )
          messages '(
                      {:message 0xdeafbeef :from 1}
                      {:message 0xdeafbeef :from 2}
                      )
          context {
                    :planets planets
                    :fleets fleets
                    :teams teams
                    :players players
                    :messages messages
                    }
          ; turn #1
          planets1 '(
                      {:id 1 :x 0 :y 0 :produced 2 :owner 1}
                      {:id 3 :x 7 :y 0 :produced 2 :owner 2}
                      {:id 2 :x 0 :y 6 :produced 2 :owner 0}
                      {:id 4 :x 7 :y 6 :produced 2 :owner 0}
                      {:id 5 :x 2 :y 3 :produced 2 :owner 0}
                      {:id 6 :x 5 :y 3 :produced 2 :owner 0}
                      )
          fleets1 '(
                     {:owner 1 :from-planet-id 1 :to-planet-id 5 :fleet 6 :arriving-by 3}
                     {:owner 2 :from-planet-id 3 :to-planet-id 6 :fleet 7 :arriving-by 3}
                     {:owner 1 :from-planet-id 1 :to-planet-id 1 :fleet 0 :arriving-by 0}
                     {:owner 2 :from-planet-id 3 :to-planet-id 3 :fleet 0 :arriving-by 0}
                     {:owner 0 :from-planet-id 2 :to-planet-id 2 :fleet 5 :arriving-by 0}
                     {:owner 0 :from-planet-id 4 :to-planet-id 4 :fleet 8 :arriving-by 0}
                     {:owner 0 :from-planet-id 5 :to-planet-id 5 :fleet 10 :arriving-by 0}
                     {:owner 0 :from-planet-id 6 :to-planet-id 6 :fleet 6 :arriving-by 0}
                     )
          teams1 '(
                    {:id 1 :name "Team1"}
                    {:id 2 :name "Team2"}
                    )
          players1 (list
                     {:id 1 :name "Player1" :team-id 1 :state (agent {})}
                     {:id 2 :name "Player2" :team-id 2 :state (agent {})}
                     )
          messages1 '(
                       {:message 0xdeafbeef :from 1}
                       {:message 0xdeafbeef :from 2}
                       )
          context1 {
                     :planets planets1
                     :fleets fleets1
                     :teams teams1
                     ;                     :players players1
                     :messages messages1
                     }
          ]
      (is (= context1 (dissoc (next-move context fn-bot2) :players)))))

  ; a bot behaviour tests

  (testing
    "next-move #1 One bot attack one neutral planet in one move. (:produced own) > (:produced attacked). Distance 3. Calculate waves."
    (let [planets '(
                     {:id 1 :x 0 :y 0 :produced 3 :owner 0}
                     {:id 2 :x 0 :y 3 :produced 6 :owner 1}
                     )
          fleets '(
                    {:owner 1 :from-planet-id 2 :to-planet-id 2 :fleet 5 :arriving-by 0}
                    {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 5 :arriving-by 0}
                    )
          teams '(
                   {:id 1 :name "Team1"}
                   {:id 2 :name "Team2"}
                   )
          players (list
                    {:id 1 :name "Player1" :team-id 1 :state (agent {})}
                    {:id 2 :name "Player2" :team-id 2 :state (agent {})}
                    )
          messages '(
                      {:message 0 :from 1}
                      )
          context {
                    :planets planets
                    :fleets fleets
                    :teams teams
                    :players players
                    :messages messages
                    }
          ; turn #1
          planets1 '(
                      {:id 1 :x 0 :y 0 :produced 3 :owner 0}
                      {:id 2 :x 0 :y 3 :produced 6 :owner 1}
                      )
          fleets1 '(
                     {:owner 1 :from-planet-id 2 :to-planet-id 1 :fleet 5 :arriving-by 2}
                     {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 5 :arriving-by 0}
                     )
          teams1 '(
                    {:id 1 :name "Team1"}
                    {:id 2 :name "Team2"}
                    )
          players1 (list
                     {:id 1 :name "Player1" :team-id 1 :state (agent {})}
                     {:id 2 :name "Player2" :team-id 2 :state (agent {})}
                     )
          messages1 '(
                       {:message 1 :from 1}
                       )
          context1 {
                     :planets planets1
                     :fleets fleets1
                     :teams teams1
                     ;                     :players players1
                     :messages messages1
                     }
          ; turn #2
          planets2 '(
                      {:id 1 :x 0 :y 0 :produced 3 :owner 0}
                      {:id 2 :x 0 :y 3 :produced 6 :owner 1}
                      )
          fleets2 '(
                     {:owner 1 :from-planet-id 2 :to-planet-id 1 :fleet 5 :arriving-by 2}
                     {:owner 0 :from-planet-id 1 :to-planet-id 1 :fleet 5 :arriving-by 0}
                     )
          teams2 '(
                    {:id 1 :name "Team1"}
                    {:id 2 :name "Team2"}
                    )
          players2 (list
                     {:id 1 :name "Player1" :team-id 1 :state (agent {})}
                     {:id 2 :name "Player2" :team-id 2 :state (agent {})}
                     )
          messages2 '(
                       {:message 1 :from 1}
                       )
          context2 {
                     :planets planets2
                     :fleets fleets2
                     :teams teams2
                     ;                     :players players2
                     :messages messages2
                     }
          ]
      (is (= context1 (dissoc (l/next-move context l/bot-decision) :players)))
      (is (= context2 (dissoc (l/next-move context1 l/bot-decision) :players))))
    )
  (testing
    "next-move #2 One bot attack one neutral planet in one move. (:produced own) < (:produced attacked). Distance 3. No attack as result."
    (let [msg ""]
      (is (= true true))))
  (testing
    "next-move #3 One bot attack one neutral planet in one move. (:produced own) = (:produced attacked). Distance 3. No attack as result."
    (let [msg ""]
      (is (= true true))))
  (testing
    "next-move #4 One bot attack one neutral planet in three move. (:produced own) > (:produced attacked). Distance 3. Calculate waves."
    (let [msg ""]
      (is (= true true))))
  (testing
    "next-move #5 One bot attack one neutral planet in three move. (:produced own) < (:produced attacked). Distance 3. No attack as result."
    (let [msg ""]
      (is (= true true))))
  (testing
    "next-move #6 One bot attack one neutral planet in three move. (:produced own) = (:produced attacked). Distance 3. No attack as result."
    (let [msg ""]
      (is (= true true))))
  (testing
    "next-move #7 One bot select most preferred neutral planet and attack it. 2 neutral planets on a map.
    (:produced own) > (:produced attacked). (:distance attacked) = 3, (:distance another) = 6. Calculate waves."
    (let [msg ""]
      (is (= true true))))
  (testing
    "next-move #8 One bot select most preferred neutral planet and attack it in two moves. 2 neutral planets on a map.
    (:produced own) > (:produced attacked). (:distance attacked) = 6, (:distance another) = 3. Calculate waves."
    (let [msg ""]
      (is (= true true))))
  (testing
    "next-move #9 One bot select most preferred neutral planet and attack it. 2 neutral planets on a map.
    (:produced own) < (:produced attacked). (:distance attacked) = 3, (:distance another) = 6. No attack as result."
    (let [msg ""]
      (is (= true true))))
  (testing
    "next-move #10 One bot select most preferred neutral planet and attack it in two moves. 2 neutral planets on a map.
    (:produced own) < (:produced attacked). (:distance attacked) = 6, (:distance another) = 3. No attack as result."
    (let [msg ""]
      (is (= true true))))
  (testing
    "next-move #11 One bot select most preferred neutral planet and attack it. 2 neutral planets on a map.
    (:produced own) = (:produced attacked). (:distance attacked) = 3, (:distance another) = 6. No attack as result."
    (let [msg ""]
      (is (= true true))))
  (testing
    "next-move #12 One bot select most preferred neutral planet and attack it in two moves. 2 neutral planets on a map.
    (:produced own) = (:produced attacked). (:distance attacked) = 6, (:distance another) = 3. No attack as result."
    (let [msg ""]
      (is (= true true))))
  (testing
    "next-move #13 One bot select most preferred neutral planet and attack it in two moves. 2 neutral planets on a map.
    (:produced own-1) > (:produced attacked). (:distance attacked) = 6, (:distance another) = 3. Two waves.
    After first wave planet's owner changed, so bot should create two-wave attack to second planet."
    (let [msg ""]
      (is (= true true))))
  (testing
    "next-move #14 One bot select most preferred neutral planet and attack it in two moves. 2 neutral planets on a map.
    Two own planets on the map. Attack can be done if distances between attacked planet and own planet are different,
    otherwise we can't send two fleet in one time.
    (:distance own-1) > (:distance own-2). Calculate delay.
    (:produced own-1) > (:produced attacked). (:distance attacked) = 6, (:distance another) = 3. Two waves."
    (let [msg ""]
      (is (= true true))))
  (testing
    "next-move #15 One bot select most preferred neutral planet and attack it in two moves. 2 neutral planets on a map.
    Two own planets on the map. Attack can be done if distances between attacked planet and own planet are different,
    otherwise we can't send two fleet in one time.
    (:distance own-1) < (:distance own-2). Calculate delay.
    (:produced own-1) > (:produced attacked). (:distance attacked) = 6, (:distance another) = 3. Two waves."
    (let [msg ""]
      (is (= true true))))
  (testing
    "next-move #16 One bot select most preferred neutral planet and attack it in two moves. 2 neutral planets on a map.
    Two own planets on the map. Attack can be done if distances between attacked planet and own planet are different,
    otherwise we can't send two fleet in one time.
    (:distance own-1) = (:distance own-2).
    (:produced own-1) > (:produced attacked). (:distance attacked) = 6, (:distance another) = 3. No attack as result."
    (let [msg ""]
      (is (= true true))))
  (testing
    "next-move #17 Two bots-teammates on a map. Bot-A select most preferred neutral planet and want to attack it. 2 neutral planets on a map.
    (:produced own-a) < (:produced attacked). (:produced own-b) < (:produced attacked).
    (:distance attacked) = 6 (:distance another) = 3. Two waves from both bots. Need to build more fleet for both bots."
    (let [msg ""]
      (is (= true true))))
  (testing
    "next-move #18 Two bots-teammates on a map. Bot-A select most preferred neutral planet and want to attack it. 2 neutral planets on a map.
    (:produced own-a) < (:produced attacked). (:produced own-b) < (:produced attacked).
    (:distance attacked) = 6 (:distance another) = 3. Two waves from both bots. Need to build more fleet for both bots.
    After sending first wave the attacked planet change its owner so the second wave should be cancelled."
    (let [msg ""]
      (is (= true true))))

  ; TODO: scenario for moves as 1) attacker and 2) attack supporter

  (testing
    ""
    (let [msg ""]
      (is (= true true))))
  )
