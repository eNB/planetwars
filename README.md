# planetwars

This is test task. Unfinished.

Need to be done: debug the logic of a bot function.


Also this code can be used as example how to create simple WebApp on Clojure.

Were used: Clojure, ClojureScript, Paper.js, Luminus + ring, clojure.test, reagent, noir, compojure. Leiningen is used for building the project. 

## Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:

    ./s-run-server.sh

To start unit tests, run:

    ./s-run-tests.sh

To use autocompiling ClojureScript feature, run:

    ./s-autobuild-cljscript.sh

## Some notes

Not only bot's logic was implemented, but also a whole game system to be able to debug a bot's behavior with enough accuracy. It's some kind of a testing board, and it's more useful to have the board to see the overall situation than to have just the bot's function and hope that it will work on the first run and the bot will do needed things in some or other situation and how effective it will be when it works as part of a team.

Probably there some bugs in the implementation is still exists, but this code will be enough to show main idea.

The "bot-decision" function is a bot's function, which implements a bot logic.

Logic:

We have a N players in a team.

1. N turns - a data exchange and retranslation a bot's ID over the bot net. The bot net is a net with the "ring" architecture. Also a bot will attack a planet which owners are still unknown.
2. One turn - no data exchange (msg = 0) over the net. On this move all the players can calculate an overall count of players in the team.
3. Next, a message will be sent to a neighbor in case of need to support an attack. Bot should calculate attack and take into account the distance between planets. It needs to be able to send fleets in two waves. So, on first turn a fleets from both players will destroy all fleet on attacked planet. Then on the next turn the attacker's fleet has more ships than the attacked planet can build, so this planet will be captured.

Priority for attack:

1. A neutral planet with "produce" < attacker's planet produce
2. An enemy planet with "produce" < attacker's planet produce
3. Any planet with "produce" >= attacker's planet produce - need to support from neighbor to attack.

Help to the neighbor has higher priority than any own actions.

## License

Copyright © 2015 Vladimir Potapevladimir Potapev