(ns planetwars.core
  (:require [reagent.core :as reagent :refer [atom]]
            [secretary.core :as secretary]
            [reagent-forms.core :refer [bind-fields]]
            [ajax.core :refer [GET POST]]
            )
  (:require-macros [secretary.core :refer [defroute]]))

; is game started?
(def state (atom {:started false}))
;
(def space-size-x (atom "20"))
;
(def space-size-y (atom "20"))
; planets count
(def planets (atom "16"))
;
(def max-produced (atom "8"))
;
(def default-fleet (atom "4"))
; teams count
(def teams (atom 0))
; players count
(def players (atom 0))
; player items list {:player1 "Team1", :player2 "Team1", :player3 "Team2", :player4 "Team2"}
(def players-list (atom {}))
;
; TODO: check that there are no an empty team
(defn is-some-team-empty []
  (let [teams-ids (set (map str (map inc (range @teams))))
        players-ids (set (map #(re-find #"\d+" (val %)) @players-list))]
    (.log js/console ">#># " teams-ids players-ids)
    (or (not= teams-ids players-ids) (empty? teams-ids) (empty? players-ids))))

(defn row [label & body]
  [:div.row [:div.col-md-2 [:span label]]
   [:div.col-md-3 body]])

(defn transform-for-draw [rsp]
  (let [sizex @space-size-x
        sizey @space-size-y
        planets (map
                  (fn [planet] (let [player (first (filter #(= (:owner planet) (:id %)) (:players rsp)))
                                     ]
                                 {:x (:x planet)
                                  :y (:y planet)
                                  :produced (:produced planet)
                                  :owner (if (zero? (:owner planet))
                                           "Neutral"
                                           (:name player))
                                  :team (if (zero? (:owner planet))
                                          "Neutral"
                                          (:name (first (filter #(= (:team-id player) (:id %)) (:teams rsp)))))
                                  :fleet (:fleet (first (filter #(and
                                                                   (= (:owner %) (:owner planet))
                                                                   (= (:from-planet-id %) (:id planet))
                                                                   (= (:to-planet-id %) (:id planet)))
                                                          (:fleets rsp))))}))
                  (:planets rsp))
        fleets-on-the-way (filter #(not= (:from-planet-id %) (:to-planet-id %)) (:fleets rsp))
        fleets (map
                 (fn [fleet]
                   (let [from-planet (first (filter #(= (:from-planet-id fleet) (:id %)) (:planets rsp)))
                         to-planet (first (filter #(= (:to-planet-id fleet) (:id %)) (:planets rsp)))
                         ]
                     {:fromx (:x from-planet)
                      :fromy (:y from-planet)
                      :tox (:x to-planet)
                      :toy (:y to-planet)
                      :arrivingby (:arriving-by fleet)
                      :owner (:name (first (filter #(= (:owner fleet) (:id %)) (:players rsp))))
                      :fleet (:fleet fleet)}
                     ))
                 fleets-on-the-way)
        ]
    {:sizex sizex :sizey sizey :planets planets :fleets fleets}))

(defn context-handler [rsp]
  (let [logs (.log js/console "Response: " (str rsp))
        draw-data (transform-for-draw rsp)
        js-draw-data (clj->js draw-data)
        logs2 (.log js/console "js-draw-data: " js-draw-data)]
    (. js/globals (drawAll js-draw-data))))

(defn context-error-handler [rsp]
  (.log js/console "Response with error: " (str rsp)))

(def form
  (fn [] [:div (row "Planetwars")
          (row "Space size X" [:input {:field :numeric :id :planets :value @space-size-x
                                       :on-change #(reset! space-size-x (-> % .-target .-value))}])
          (row "Space size Y" [:input {:field :numeric :id :planets :value @space-size-y
                                       :on-change #(reset! space-size-y (-> % .-target .-value))}])
          (row "Planets" [:input {:field :numeric :id :planets :value @planets
                                  :on-change #(reset! planets (-> % .-target .-value))}])
          (row "Max produced fleet" [:input {:field :numeric :id :planets :value @max-produced
                                             :on-change #(reset! max-produced (-> % .-target .-value))}])
          (row "Default fleet" [:input {:field :numeric :id :planets :value @default-fleet
                                        :on-change #(reset! default-fleet (-> % .-target .-value))}])
          (row "Teams" [:input {:field :numeric :id :teams :value @teams
                                :on-change #(do
                                              (reset! players-list {})
                                              (for [n (range @players)]
                                                (swap! players-list assoc (keyword (str "player" (inc n))) "Team1"))
                                              (reset! teams (-> % .-target .-value)))}])
          (row "Players" [:input {:field :numeric :id :players :value @players
                                  :on-change #(do
                                                (.log js/console "Point 1 fired")
                                                (reset! players-list {})
                                                (for [n (range @players)]
                                                  (swap! players-list assoc (keyword (str "player" (inc n))) "Team1")) ; TODO: remove copypaste
                                                (reset! players (-> % .-target .-value)))}])
          ; TODO: check later the situation of (for [n (range 1 (+ 1 @players))]... or (for [n (range 1 (inc @players))]... . It works as not expected.
          ; TODO: Seems that it can be a bug in ClojureScript compiler.
          (for [n (range @players)]
            (let [player-caption (str "Player" (inc n))]
              (.log js/console "Point 3 fired")
              (swap! players-list assoc (keyword (str "player" (inc n))) "Team1")
              (row player-caption
                [:select {:field :list
                          :size 1
                          :id (keyword player-caption)
                          :on-change #(do
                                        (.log js/console "Point 2 fired")
                                        (swap! players-list assoc (keyword (str "player" (inc n))) (-> % .-target .-value)))}
                 (for [team-id (range @teams)]
                   [:option {:key (keyword (str "id" (inc team-id)))} (str "Team" (inc team-id))])
                 ])))
          (row "" [:button {:on-click #(let [data-to-send {:space-size-x @space-size-x
                                                           :space-size-y @space-size-y
                                                           :num-of-planets @planets
                                                           :max-produced @max-produced
                                                           :default-fleet @default-fleet
                                                           :num-of-teams @teams
                                                           :players-list @players-list}]
                                         (if (is-some-team-empty)
                                           (js/alert "Error: Some team has no players")
                                           (do
                                             (POST "/init" {:format :edn :params data-to-send})
                                             (GET "/context" {:handler context-handler :error-handler context-error-handler}))))}
                   "Init game"])
          (row "" [:button {:on-click #(do
                                         (GET "/next")
                                         (GET "/context" {:handler context-handler :error-handler context-error-handler}))}
                   "Next move"])
          ]))

; TODO: check that there no empty teams. And also need validate all fields.

(secretary/set-config! :prefix "#")

(defroute "/" [] (.log js/console "hi!"))

(defn label [text]
  [:label text])
(defn input-field [label-text id]
  (let [value (atom nil)]
    (fn []
      [:div [label "The value is: " @value]
       [:input {:type "text"
                :value @value
                :on-change #(reset! value (-> % .-target .-value))}]])))
(defn init! []
  ;(reagent/render-component [input-field] (.-body js/document))
  (reagent/render-component [form] (.getElementById js/document "form1")))
