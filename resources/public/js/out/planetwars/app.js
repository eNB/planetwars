// Compiled by ClojureScript 0.0-2644 {}
if(!goog.isProvided_('planetwars.app')) {
goog.provide('planetwars.app');
}
goog.require('cljs.core');
goog.require('planetwars.core');
cljs.core.enable_console_print_BANG_.call(null);
planetwars.core.init_BANG_.call(null);

//# sourceMappingURL=app.js.map