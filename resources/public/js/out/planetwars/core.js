// Compiled by ClojureScript 0.0-2644 {}
if(!goog.isProvided_('planetwars.core')) {
goog.provide('planetwars.core');
}
goog.require('cljs.core');
goog.require('ajax.core');
goog.require('reagent_forms.core');
goog.require('secretary.core');
goog.require('reagent.core');
planetwars.core.state = reagent.core.atom.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"started","started",585705024),false], null));
planetwars.core.space_size_x = reagent.core.atom.call(null,"20");
planetwars.core.space_size_y = reagent.core.atom.call(null,"20");
planetwars.core.planets = reagent.core.atom.call(null,"16");
planetwars.core.max_produced = reagent.core.atom.call(null,"8");
planetwars.core.default_fleet = reagent.core.atom.call(null,"4");
planetwars.core.teams = reagent.core.atom.call(null,(0));
planetwars.core.players = reagent.core.atom.call(null,(0));
planetwars.core.players_list = reagent.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
planetwars.core.is_some_team_empty = (function is_some_team_empty(){
var teams_ids = cljs.core.set.call(null,cljs.core.map.call(null,cljs.core.str,cljs.core.map.call(null,cljs.core.inc,cljs.core.range.call(null,cljs.core.deref.call(null,planetwars.core.teams)))));
var players_ids = cljs.core.set.call(null,cljs.core.map.call(null,((function (teams_ids){
return (function (p1__6542_SHARP_){
return cljs.core.re_find.call(null,/\d+/,cljs.core.val.call(null,p1__6542_SHARP_));
});})(teams_ids))
,cljs.core.deref.call(null,planetwars.core.players_list)));
console.log(">#># ",teams_ids,players_ids);

return (cljs.core.not_EQ_.call(null,teams_ids,players_ids)) || (cljs.core.empty_QMARK_.call(null,teams_ids)) || (cljs.core.empty_QMARK_.call(null,players_ids));
});
/**
* @param {...*} var_args
*/
planetwars.core.row = (function() { 
var row__delegate = function (label,body){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.row","div.row",133678515),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.col-md-2","div.col-md-2",-138798418),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),label], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.col-md-3","div.col-md-3",1386112129),body], null)], null);
};
var row = function (label,var_args){
var body = null;
if (arguments.length > 1) {
  body = cljs.core.array_seq(Array.prototype.slice.call(arguments, 1),0);
} 
return row__delegate.call(this,label,body);};
row.cljs$lang$maxFixedArity = 1;
row.cljs$lang$applyTo = (function (arglist__6543){
var label = cljs.core.first(arglist__6543);
var body = cljs.core.rest(arglist__6543);
return row__delegate(label,body);
});
row.cljs$core$IFn$_invoke$arity$variadic = row__delegate;
return row;
})()
;
planetwars.core.transform_for_draw = (function transform_for_draw(rsp){
var sizex = cljs.core.deref.call(null,planetwars.core.space_size_x);
var sizey = cljs.core.deref.call(null,planetwars.core.space_size_y);
var planets = cljs.core.map.call(null,((function (sizex,sizey){
return (function (planet){
var player = cljs.core.first.call(null,cljs.core.filter.call(null,((function (sizex,sizey){
return (function (p1__6544_SHARP_){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"owner","owner",-392611939).cljs$core$IFn$_invoke$arity$1(planet),new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(p1__6544_SHARP_));
});})(sizex,sizey))
,new cljs.core.Keyword(null,"players","players",-1361554569).cljs$core$IFn$_invoke$arity$1(rsp)));
return new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"x","x",2099068185),new cljs.core.Keyword(null,"x","x",2099068185).cljs$core$IFn$_invoke$arity$1(planet),new cljs.core.Keyword(null,"y","y",-1757859776),new cljs.core.Keyword(null,"y","y",-1757859776).cljs$core$IFn$_invoke$arity$1(planet),new cljs.core.Keyword(null,"produced","produced",-1483746182),new cljs.core.Keyword(null,"produced","produced",-1483746182).cljs$core$IFn$_invoke$arity$1(planet),new cljs.core.Keyword(null,"owner","owner",-392611939),(((new cljs.core.Keyword(null,"owner","owner",-392611939).cljs$core$IFn$_invoke$arity$1(planet) === (0)))?"Neutral":new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(player)),new cljs.core.Keyword(null,"team","team",1355747699),(((new cljs.core.Keyword(null,"owner","owner",-392611939).cljs$core$IFn$_invoke$arity$1(planet) === (0)))?"Neutral":new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(cljs.core.first.call(null,cljs.core.filter.call(null,((function (player,sizex,sizey){
return (function (p1__6545_SHARP_){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"team-id","team-id",-14505725).cljs$core$IFn$_invoke$arity$1(player),new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(p1__6545_SHARP_));
});})(player,sizex,sizey))
,new cljs.core.Keyword(null,"teams","teams",1677714510).cljs$core$IFn$_invoke$arity$1(rsp))))),new cljs.core.Keyword(null,"fleet","fleet",-1325121006),new cljs.core.Keyword(null,"fleet","fleet",-1325121006).cljs$core$IFn$_invoke$arity$1(cljs.core.first.call(null,cljs.core.filter.call(null,((function (player,sizex,sizey){
return (function (p1__6546_SHARP_){
return (cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"owner","owner",-392611939).cljs$core$IFn$_invoke$arity$1(p1__6546_SHARP_),new cljs.core.Keyword(null,"owner","owner",-392611939).cljs$core$IFn$_invoke$arity$1(planet))) && (cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"from-planet-id","from-planet-id",-1535174887).cljs$core$IFn$_invoke$arity$1(p1__6546_SHARP_),new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(planet))) && (cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"to-planet-id","to-planet-id",-918831080).cljs$core$IFn$_invoke$arity$1(p1__6546_SHARP_),new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(planet)));
});})(player,sizex,sizey))
,new cljs.core.Keyword(null,"fleets","fleets",-1571868084).cljs$core$IFn$_invoke$arity$1(rsp))))], null);
});})(sizex,sizey))
,new cljs.core.Keyword(null,"planets","planets",1711765443).cljs$core$IFn$_invoke$arity$1(rsp));
var fleets_on_the_way = cljs.core.filter.call(null,((function (sizex,sizey,planets){
return (function (p1__6547_SHARP_){
return cljs.core.not_EQ_.call(null,new cljs.core.Keyword(null,"from-planet-id","from-planet-id",-1535174887).cljs$core$IFn$_invoke$arity$1(p1__6547_SHARP_),new cljs.core.Keyword(null,"to-planet-id","to-planet-id",-918831080).cljs$core$IFn$_invoke$arity$1(p1__6547_SHARP_));
});})(sizex,sizey,planets))
,new cljs.core.Keyword(null,"fleets","fleets",-1571868084).cljs$core$IFn$_invoke$arity$1(rsp));
var fleets = cljs.core.map.call(null,((function (sizex,sizey,planets,fleets_on_the_way){
return (function (fleet){
var from_planet = cljs.core.first.call(null,cljs.core.filter.call(null,((function (sizex,sizey,planets,fleets_on_the_way){
return (function (p1__6548_SHARP_){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"from-planet-id","from-planet-id",-1535174887).cljs$core$IFn$_invoke$arity$1(fleet),new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(p1__6548_SHARP_));
});})(sizex,sizey,planets,fleets_on_the_way))
,new cljs.core.Keyword(null,"planets","planets",1711765443).cljs$core$IFn$_invoke$arity$1(rsp)));
var to_planet = cljs.core.first.call(null,cljs.core.filter.call(null,((function (from_planet,sizex,sizey,planets,fleets_on_the_way){
return (function (p1__6549_SHARP_){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"to-planet-id","to-planet-id",-918831080).cljs$core$IFn$_invoke$arity$1(fleet),new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(p1__6549_SHARP_));
});})(from_planet,sizex,sizey,planets,fleets_on_the_way))
,new cljs.core.Keyword(null,"planets","planets",1711765443).cljs$core$IFn$_invoke$arity$1(rsp)));
return new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword(null,"fromx","fromx",346760477),new cljs.core.Keyword(null,"x","x",2099068185).cljs$core$IFn$_invoke$arity$1(from_planet),new cljs.core.Keyword(null,"fromy","fromy",-794847750),new cljs.core.Keyword(null,"y","y",-1757859776).cljs$core$IFn$_invoke$arity$1(from_planet),new cljs.core.Keyword(null,"tox","tox",-20457648),new cljs.core.Keyword(null,"x","x",2099068185).cljs$core$IFn$_invoke$arity$1(to_planet),new cljs.core.Keyword(null,"toy","toy",-1189798686),new cljs.core.Keyword(null,"y","y",-1757859776).cljs$core$IFn$_invoke$arity$1(to_planet),new cljs.core.Keyword(null,"arrivingby","arrivingby",-327205972),new cljs.core.Keyword(null,"arriving-by","arriving-by",248466407).cljs$core$IFn$_invoke$arity$1(fleet),new cljs.core.Keyword(null,"owner","owner",-392611939),new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(cljs.core.first.call(null,cljs.core.filter.call(null,((function (from_planet,to_planet,sizex,sizey,planets,fleets_on_the_way){
return (function (p1__6550_SHARP_){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"owner","owner",-392611939).cljs$core$IFn$_invoke$arity$1(fleet),new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(p1__6550_SHARP_));
});})(from_planet,to_planet,sizex,sizey,planets,fleets_on_the_way))
,new cljs.core.Keyword(null,"players","players",-1361554569).cljs$core$IFn$_invoke$arity$1(rsp)))),new cljs.core.Keyword(null,"fleet","fleet",-1325121006),new cljs.core.Keyword(null,"fleet","fleet",-1325121006).cljs$core$IFn$_invoke$arity$1(fleet)], null);
});})(sizex,sizey,planets,fleets_on_the_way))
,fleets_on_the_way);
return new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"sizex","sizex",1201177545),sizex,new cljs.core.Keyword(null,"sizey","sizey",1539605282),sizey,new cljs.core.Keyword(null,"planets","planets",1711765443),planets,new cljs.core.Keyword(null,"fleets","fleets",-1571868084),fleets], null);
});
planetwars.core.context_handler = (function context_handler(rsp){
var logs = console.log("Response: ",[cljs.core.str(rsp)].join(''));
var draw_data = planetwars.core.transform_for_draw.call(null,rsp);
var js_draw_data = cljs.core.clj__GT_js.call(null,draw_data);
var logs2 = console.log("js-draw-data: ",js_draw_data);
return globals.drawAll(js_draw_data);
});
planetwars.core.context_error_handler = (function context_error_handler(rsp){
return console.log("Response with error: ",[cljs.core.str(rsp)].join(''));
});
planetwars.core.form = (function form(){
return new cljs.core.PersistentVector(null, 12, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),planetwars.core.row.call(null,"Planetwars"),planetwars.core.row.call(null,"Space size X",new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"field","field",-1302436500),new cljs.core.Keyword(null,"numeric","numeric",-1495594714),new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"planets","planets",1711765443),new cljs.core.Keyword(null,"value","value",305978217),cljs.core.deref.call(null,planetwars.core.space_size_x),new cljs.core.Keyword(null,"on-change","on-change",-732046149),(function (p1__6551_SHARP_){
return cljs.core.reset_BANG_.call(null,planetwars.core.space_size_x,p1__6551_SHARP_.target.value);
})], null)], null)),planetwars.core.row.call(null,"Space size Y",new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"field","field",-1302436500),new cljs.core.Keyword(null,"numeric","numeric",-1495594714),new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"planets","planets",1711765443),new cljs.core.Keyword(null,"value","value",305978217),cljs.core.deref.call(null,planetwars.core.space_size_y),new cljs.core.Keyword(null,"on-change","on-change",-732046149),(function (p1__6552_SHARP_){
return cljs.core.reset_BANG_.call(null,planetwars.core.space_size_y,p1__6552_SHARP_.target.value);
})], null)], null)),planetwars.core.row.call(null,"Planets",new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"field","field",-1302436500),new cljs.core.Keyword(null,"numeric","numeric",-1495594714),new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"planets","planets",1711765443),new cljs.core.Keyword(null,"value","value",305978217),cljs.core.deref.call(null,planetwars.core.planets),new cljs.core.Keyword(null,"on-change","on-change",-732046149),(function (p1__6553_SHARP_){
return cljs.core.reset_BANG_.call(null,planetwars.core.planets,p1__6553_SHARP_.target.value);
})], null)], null)),planetwars.core.row.call(null,"Max produced fleet",new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"field","field",-1302436500),new cljs.core.Keyword(null,"numeric","numeric",-1495594714),new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"planets","planets",1711765443),new cljs.core.Keyword(null,"value","value",305978217),cljs.core.deref.call(null,planetwars.core.max_produced),new cljs.core.Keyword(null,"on-change","on-change",-732046149),(function (p1__6554_SHARP_){
return cljs.core.reset_BANG_.call(null,planetwars.core.max_produced,p1__6554_SHARP_.target.value);
})], null)], null)),planetwars.core.row.call(null,"Default fleet",new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"field","field",-1302436500),new cljs.core.Keyword(null,"numeric","numeric",-1495594714),new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"planets","planets",1711765443),new cljs.core.Keyword(null,"value","value",305978217),cljs.core.deref.call(null,planetwars.core.default_fleet),new cljs.core.Keyword(null,"on-change","on-change",-732046149),(function (p1__6555_SHARP_){
return cljs.core.reset_BANG_.call(null,planetwars.core.default_fleet,p1__6555_SHARP_.target.value);
})], null)], null)),planetwars.core.row.call(null,"Teams",new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"field","field",-1302436500),new cljs.core.Keyword(null,"numeric","numeric",-1495594714),new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"teams","teams",1677714510),new cljs.core.Keyword(null,"value","value",305978217),cljs.core.deref.call(null,planetwars.core.teams),new cljs.core.Keyword(null,"on-change","on-change",-732046149),(function (p1__6556_SHARP_){
cljs.core.reset_BANG_.call(null,planetwars.core.players_list,cljs.core.PersistentArrayMap.EMPTY);

var iter__4477__auto___6615 = (function iter__6587(s__6588){
return (new cljs.core.LazySeq(null,(function (){
var s__6588__$1 = s__6588;
while(true){
var temp__4126__auto__ = cljs.core.seq.call(null,s__6588__$1);
if(temp__4126__auto__){
var s__6588__$2 = temp__4126__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__6588__$2)){
var c__4475__auto__ = cljs.core.chunk_first.call(null,s__6588__$2);
var size__4476__auto__ = cljs.core.count.call(null,c__4475__auto__);
var b__6590 = cljs.core.chunk_buffer.call(null,size__4476__auto__);
if((function (){var i__6589 = (0);
while(true){
if((i__6589 < size__4476__auto__)){
var n = cljs.core._nth.call(null,c__4475__auto__,i__6589);
cljs.core.chunk_append.call(null,b__6590,cljs.core.swap_BANG_.call(null,planetwars.core.players_list,cljs.core.assoc,cljs.core.keyword.call(null,[cljs.core.str("player"),cljs.core.str((n + (1)))].join('')),"Team1"));

var G__6616 = (i__6589 + (1));
i__6589 = G__6616;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__6590),iter__6587.call(null,cljs.core.chunk_rest.call(null,s__6588__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__6590),null);
}
} else {
var n = cljs.core.first.call(null,s__6588__$2);
return cljs.core.cons.call(null,cljs.core.swap_BANG_.call(null,planetwars.core.players_list,cljs.core.assoc,cljs.core.keyword.call(null,[cljs.core.str("player"),cljs.core.str((n + (1)))].join('')),"Team1"),iter__6587.call(null,cljs.core.rest.call(null,s__6588__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
iter__4477__auto___6615.call(null,cljs.core.range.call(null,cljs.core.deref.call(null,planetwars.core.players)));

return cljs.core.reset_BANG_.call(null,planetwars.core.teams,p1__6556_SHARP_.target.value);
})], null)], null)),planetwars.core.row.call(null,"Players",new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"field","field",-1302436500),new cljs.core.Keyword(null,"numeric","numeric",-1495594714),new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"players","players",-1361554569),new cljs.core.Keyword(null,"value","value",305978217),cljs.core.deref.call(null,planetwars.core.players),new cljs.core.Keyword(null,"on-change","on-change",-732046149),(function (p1__6557_SHARP_){
console.log("Point 1 fired");

cljs.core.reset_BANG_.call(null,planetwars.core.players_list,cljs.core.PersistentArrayMap.EMPTY);

var iter__4477__auto___6617 = (function iter__6591(s__6592){
return (new cljs.core.LazySeq(null,(function (){
var s__6592__$1 = s__6592;
while(true){
var temp__4126__auto__ = cljs.core.seq.call(null,s__6592__$1);
if(temp__4126__auto__){
var s__6592__$2 = temp__4126__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__6592__$2)){
var c__4475__auto__ = cljs.core.chunk_first.call(null,s__6592__$2);
var size__4476__auto__ = cljs.core.count.call(null,c__4475__auto__);
var b__6594 = cljs.core.chunk_buffer.call(null,size__4476__auto__);
if((function (){var i__6593 = (0);
while(true){
if((i__6593 < size__4476__auto__)){
var n = cljs.core._nth.call(null,c__4475__auto__,i__6593);
cljs.core.chunk_append.call(null,b__6594,cljs.core.swap_BANG_.call(null,planetwars.core.players_list,cljs.core.assoc,cljs.core.keyword.call(null,[cljs.core.str("player"),cljs.core.str((n + (1)))].join('')),"Team1"));

var G__6618 = (i__6593 + (1));
i__6593 = G__6618;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__6594),iter__6591.call(null,cljs.core.chunk_rest.call(null,s__6592__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__6594),null);
}
} else {
var n = cljs.core.first.call(null,s__6592__$2);
return cljs.core.cons.call(null,cljs.core.swap_BANG_.call(null,planetwars.core.players_list,cljs.core.assoc,cljs.core.keyword.call(null,[cljs.core.str("player"),cljs.core.str((n + (1)))].join('')),"Team1"),iter__6591.call(null,cljs.core.rest.call(null,s__6592__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
iter__4477__auto___6617.call(null,cljs.core.range.call(null,cljs.core.deref.call(null,planetwars.core.players)));

return cljs.core.reset_BANG_.call(null,planetwars.core.players,p1__6557_SHARP_.target.value);
})], null)], null)),(function (){var iter__4477__auto__ = (function iter__6595(s__6596){
return (new cljs.core.LazySeq(null,(function (){
var s__6596__$1 = s__6596;
while(true){
var temp__4126__auto__ = cljs.core.seq.call(null,s__6596__$1);
if(temp__4126__auto__){
var s__6596__$2 = temp__4126__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__6596__$2)){
var c__4475__auto__ = cljs.core.chunk_first.call(null,s__6596__$2);
var size__4476__auto__ = cljs.core.count.call(null,c__4475__auto__);
var b__6598 = cljs.core.chunk_buffer.call(null,size__4476__auto__);
if((function (){var i__6597 = (0);
while(true){
if((i__6597 < size__4476__auto__)){
var n = cljs.core._nth.call(null,c__4475__auto__,i__6597);
cljs.core.chunk_append.call(null,b__6598,(function (){var player_caption = [cljs.core.str("Player"),cljs.core.str((n + (1)))].join('');
console.log("Point 3 fired");

cljs.core.swap_BANG_.call(null,planetwars.core.players_list,cljs.core.assoc,cljs.core.keyword.call(null,[cljs.core.str("player"),cljs.core.str((n + (1)))].join('')),"Team1");

return planetwars.core.row.call(null,player_caption,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"select","select",1147833503),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"field","field",-1302436500),new cljs.core.Keyword(null,"list","list",765357683),new cljs.core.Keyword(null,"size","size",1098693007),(1),new cljs.core.Keyword(null,"id","id",-1388402092),cljs.core.keyword.call(null,player_caption),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (i__6597,player_caption,n,c__4475__auto__,size__4476__auto__,b__6598,s__6596__$2,temp__4126__auto__){
return (function (p1__6558_SHARP_){
console.log("Point 2 fired");

return cljs.core.swap_BANG_.call(null,planetwars.core.players_list,cljs.core.assoc,cljs.core.keyword.call(null,[cljs.core.str("player"),cljs.core.str((n + (1)))].join('')),p1__6558_SHARP_.target.value);
});})(i__6597,player_caption,n,c__4475__auto__,size__4476__auto__,b__6598,s__6596__$2,temp__4126__auto__))
], null),(function (){var iter__4477__auto__ = ((function (i__6597,player_caption,n,c__4475__auto__,size__4476__auto__,b__6598,s__6596__$2,temp__4126__auto__){
return (function iter__6607(s__6608){
return (new cljs.core.LazySeq(null,((function (i__6597,player_caption,n,c__4475__auto__,size__4476__auto__,b__6598,s__6596__$2,temp__4126__auto__){
return (function (){
var s__6608__$1 = s__6608;
while(true){
var temp__4126__auto____$1 = cljs.core.seq.call(null,s__6608__$1);
if(temp__4126__auto____$1){
var s__6608__$2 = temp__4126__auto____$1;
if(cljs.core.chunked_seq_QMARK_.call(null,s__6608__$2)){
var c__4475__auto____$1 = cljs.core.chunk_first.call(null,s__6608__$2);
var size__4476__auto____$1 = cljs.core.count.call(null,c__4475__auto____$1);
var b__6610 = cljs.core.chunk_buffer.call(null,size__4476__auto____$1);
if((function (){var i__6609 = (0);
while(true){
if((i__6609 < size__4476__auto____$1)){
var team_id = cljs.core._nth.call(null,c__4475__auto____$1,i__6609);
cljs.core.chunk_append.call(null,b__6610,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),cljs.core.keyword.call(null,[cljs.core.str("id"),cljs.core.str((team_id + (1)))].join(''))], null),[cljs.core.str("Team"),cljs.core.str((team_id + (1)))].join('')], null));

var G__6619 = (i__6609 + (1));
i__6609 = G__6619;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__6610),iter__6607.call(null,cljs.core.chunk_rest.call(null,s__6608__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__6610),null);
}
} else {
var team_id = cljs.core.first.call(null,s__6608__$2);
return cljs.core.cons.call(null,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),cljs.core.keyword.call(null,[cljs.core.str("id"),cljs.core.str((team_id + (1)))].join(''))], null),[cljs.core.str("Team"),cljs.core.str((team_id + (1)))].join('')], null),iter__6607.call(null,cljs.core.rest.call(null,s__6608__$2)));
}
} else {
return null;
}
break;
}
});})(i__6597,player_caption,n,c__4475__auto__,size__4476__auto__,b__6598,s__6596__$2,temp__4126__auto__))
,null,null));
});})(i__6597,player_caption,n,c__4475__auto__,size__4476__auto__,b__6598,s__6596__$2,temp__4126__auto__))
;
return iter__4477__auto__.call(null,cljs.core.range.call(null,cljs.core.deref.call(null,planetwars.core.teams)));
})()], null));
})());

var G__6620 = (i__6597 + (1));
i__6597 = G__6620;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__6598),iter__6595.call(null,cljs.core.chunk_rest.call(null,s__6596__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__6598),null);
}
} else {
var n = cljs.core.first.call(null,s__6596__$2);
return cljs.core.cons.call(null,(function (){var player_caption = [cljs.core.str("Player"),cljs.core.str((n + (1)))].join('');
console.log("Point 3 fired");

cljs.core.swap_BANG_.call(null,planetwars.core.players_list,cljs.core.assoc,cljs.core.keyword.call(null,[cljs.core.str("player"),cljs.core.str((n + (1)))].join('')),"Team1");

return planetwars.core.row.call(null,player_caption,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"select","select",1147833503),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"field","field",-1302436500),new cljs.core.Keyword(null,"list","list",765357683),new cljs.core.Keyword(null,"size","size",1098693007),(1),new cljs.core.Keyword(null,"id","id",-1388402092),cljs.core.keyword.call(null,player_caption),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (player_caption,n,s__6596__$2,temp__4126__auto__){
return (function (p1__6558_SHARP_){
console.log("Point 2 fired");

return cljs.core.swap_BANG_.call(null,planetwars.core.players_list,cljs.core.assoc,cljs.core.keyword.call(null,[cljs.core.str("player"),cljs.core.str((n + (1)))].join('')),p1__6558_SHARP_.target.value);
});})(player_caption,n,s__6596__$2,temp__4126__auto__))
], null),(function (){var iter__4477__auto__ = ((function (player_caption,n,s__6596__$2,temp__4126__auto__){
return (function iter__6611(s__6612){
return (new cljs.core.LazySeq(null,((function (player_caption,n,s__6596__$2,temp__4126__auto__){
return (function (){
var s__6612__$1 = s__6612;
while(true){
var temp__4126__auto____$1 = cljs.core.seq.call(null,s__6612__$1);
if(temp__4126__auto____$1){
var s__6612__$2 = temp__4126__auto____$1;
if(cljs.core.chunked_seq_QMARK_.call(null,s__6612__$2)){
var c__4475__auto__ = cljs.core.chunk_first.call(null,s__6612__$2);
var size__4476__auto__ = cljs.core.count.call(null,c__4475__auto__);
var b__6614 = cljs.core.chunk_buffer.call(null,size__4476__auto__);
if((function (){var i__6613 = (0);
while(true){
if((i__6613 < size__4476__auto__)){
var team_id = cljs.core._nth.call(null,c__4475__auto__,i__6613);
cljs.core.chunk_append.call(null,b__6614,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),cljs.core.keyword.call(null,[cljs.core.str("id"),cljs.core.str((team_id + (1)))].join(''))], null),[cljs.core.str("Team"),cljs.core.str((team_id + (1)))].join('')], null));

var G__6621 = (i__6613 + (1));
i__6613 = G__6621;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__6614),iter__6611.call(null,cljs.core.chunk_rest.call(null,s__6612__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__6614),null);
}
} else {
var team_id = cljs.core.first.call(null,s__6612__$2);
return cljs.core.cons.call(null,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),cljs.core.keyword.call(null,[cljs.core.str("id"),cljs.core.str((team_id + (1)))].join(''))], null),[cljs.core.str("Team"),cljs.core.str((team_id + (1)))].join('')], null),iter__6611.call(null,cljs.core.rest.call(null,s__6612__$2)));
}
} else {
return null;
}
break;
}
});})(player_caption,n,s__6596__$2,temp__4126__auto__))
,null,null));
});})(player_caption,n,s__6596__$2,temp__4126__auto__))
;
return iter__4477__auto__.call(null,cljs.core.range.call(null,cljs.core.deref.call(null,planetwars.core.teams)));
})()], null));
})(),iter__6595.call(null,cljs.core.rest.call(null,s__6596__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4477__auto__.call(null,cljs.core.range.call(null,cljs.core.deref.call(null,planetwars.core.players)));
})(),planetwars.core.row.call(null,"",new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button","button",1456579943),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),(function (){
var data_to_send = new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword(null,"space-size-x","space-size-x",1828377095),cljs.core.deref.call(null,planetwars.core.space_size_x),new cljs.core.Keyword(null,"space-size-y","space-size-y",-1385729374),cljs.core.deref.call(null,planetwars.core.space_size_y),new cljs.core.Keyword(null,"num-of-planets","num-of-planets",1313827247),cljs.core.deref.call(null,planetwars.core.planets),new cljs.core.Keyword(null,"max-produced","max-produced",-92475070),cljs.core.deref.call(null,planetwars.core.max_produced),new cljs.core.Keyword(null,"default-fleet","default-fleet",1189402318),cljs.core.deref.call(null,planetwars.core.default_fleet),new cljs.core.Keyword(null,"num-of-teams","num-of-teams",623632478),cljs.core.deref.call(null,planetwars.core.teams),new cljs.core.Keyword(null,"players-list","players-list",-1808942460),cljs.core.deref.call(null,planetwars.core.players_list)], null);
if(planetwars.core.is_some_team_empty.call(null)){
return alert("Error: Some team has no players");
} else {
ajax.core.POST.call(null,"/init",new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"format","format",-1306924766),new cljs.core.Keyword(null,"edn","edn",1317840885),new cljs.core.Keyword(null,"params","params",710516235),data_to_send], null));

return ajax.core.GET.call(null,"/context",new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"handler","handler",-195596612),planetwars.core.context_handler,new cljs.core.Keyword(null,"error-handler","error-handler",-484945776),planetwars.core.context_error_handler], null));
}
})], null),"Init game"], null)),planetwars.core.row.call(null,"",new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button","button",1456579943),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),(function (){
ajax.core.GET.call(null,"/next");

return ajax.core.GET.call(null,"/context",new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"handler","handler",-195596612),planetwars.core.context_handler,new cljs.core.Keyword(null,"error-handler","error-handler",-484945776),planetwars.core.context_error_handler], null));
})], null),"Next move"], null))], null);
});
secretary.core.set_config_BANG_.call(null,new cljs.core.Keyword(null,"prefix","prefix",-265908465),"#");
var action__6450__auto___6624 = (function (params__6451__auto__){
if(cljs.core.map_QMARK_.call(null,params__6451__auto__)){
var map__6622 = params__6451__auto__;
var map__6622__$1 = ((cljs.core.seq_QMARK_.call(null,map__6622))?cljs.core.apply.call(null,cljs.core.hash_map,map__6622):map__6622);
return console.log("hi!");
} else {
if(cljs.core.vector_QMARK_.call(null,params__6451__auto__)){
var vec__6623 = params__6451__auto__;
return console.log("hi!");
} else {
return null;
}
}
});
secretary.core.add_route_BANG_.call(null,"/",action__6450__auto___6624);

planetwars.core.label = (function label(text){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label","label",1718410804),text], null);
});
planetwars.core.input_field = (function input_field(label_text,id){
var value = reagent.core.atom.call(null,null);
return ((function (value){
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [planetwars.core.label,"The value is: ",cljs.core.deref.call(null,value)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),"text",new cljs.core.Keyword(null,"value","value",305978217),cljs.core.deref.call(null,value),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (value){
return (function (p1__6625_SHARP_){
return cljs.core.reset_BANG_.call(null,value,p1__6625_SHARP_.target.value);
});})(value))
], null)], null)], null);
});
;})(value))
});
planetwars.core.init_BANG_ = (function init_BANG_(){
return reagent.core.render_component.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [planetwars.core.form], null),document.getElementById("form1"));
});

//# sourceMappingURL=core.js.map