// Compiled by ClojureScript 0.0-2644 {}
goog.provide('safenote.core');
goog.require('cljs.core');
goog.require('ajax.core');
goog.require('reagent_forms.core');
goog.require('secretary.core');
goog.require('reagent.core');
safenote.core.state = reagent.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 1, [cljs.core.constant$keyword$164,false], null));
/**
* @param {...*} var_args
*/
safenote.core.row = (function() { 
var row__delegate = function (label,body){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$165,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$166,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$167,label], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$168,body], null)], null);
};
var row = function (label,var_args){
var body = null;
if (arguments.length > 1) {
  body = cljs.core.array_seq(Array.prototype.slice.call(arguments, 1),0);
} 
return row__delegate.call(this,label,body);};
row.cljs$lang$maxFixedArity = 1;
row.cljs$lang$applyTo = (function (arglist__11845){
var label = cljs.core.first(arglist__11845);
var body = cljs.core.rest(arglist__11845);
return row__delegate(label,body);
});
row.cljs$core$IFn$_invoke$arity$variadic = row__delegate;
return row;
})()
;
safenote.core.text_input = (function text_input(id,label){
return safenote.core.row.cljs$core$IFn$_invoke$arity$variadic(label,cljs.core.array_seq([new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$128,new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$110,cljs.core.constant$keyword$111,cljs.core.constant$keyword$28,id], null)], null)], 0));
});
/**
* @param {...*} var_args
*/
safenote.core.selection_list = (function() { 
var selection_list__delegate = function (label,id,items){
return safenote.core.row.cljs$core$IFn$_invoke$arity$variadic(label,cljs.core.array_seq([new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$169,new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$110,cljs.core.constant$keyword$151,cljs.core.constant$keyword$28,id], null),(function (){var iter__4477__auto__ = (function iter__11856(s__11857){
return (new cljs.core.LazySeq(null,(function (){
var s__11857__$1 = s__11857;
while(true){
var temp__4126__auto__ = cljs.core.seq(s__11857__$1);
if(temp__4126__auto__){
var s__11857__$2 = temp__4126__auto__;
if(cljs.core.chunked_seq_QMARK_(s__11857__$2)){
var c__4475__auto__ = cljs.core.chunk_first(s__11857__$2);
var size__4476__auto__ = cljs.core.count(c__4475__auto__);
var b__11859 = cljs.core.chunk_buffer(size__4476__auto__);
if((function (){var i__11858 = (0);
while(true){
if((i__11858 < size__4476__auto__)){
var vec__11864 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4475__auto__,i__11858);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11864,(0),null);
var label__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11864,(1),null);
cljs.core.chunk_append(b__11859,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$170,new cljs.core.PersistentArrayMap(null, 1, [cljs.core.constant$keyword$76,k], null),label__$1], null));

var G__11866 = (i__11858 + (1));
i__11858 = G__11866;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__11859),iter__11856(cljs.core.chunk_rest(s__11857__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__11859),null);
}
} else {
var vec__11865 = cljs.core.first(s__11857__$2);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11865,(0),null);
var label__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11865,(1),null);
return cljs.core.cons(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$170,new cljs.core.PersistentArrayMap(null, 1, [cljs.core.constant$keyword$76,k], null),label__$1], null),iter__11856(cljs.core.rest(s__11857__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4477__auto__(items);
})()], null)], 0));
};
var selection_list = function (label,id,var_args){
var items = null;
if (arguments.length > 2) {
  items = cljs.core.array_seq(Array.prototype.slice.call(arguments, 2),0);
} 
return selection_list__delegate.call(this,label,id,items);};
selection_list.cljs$lang$maxFixedArity = 2;
selection_list.cljs$lang$applyTo = (function (arglist__11867){
var label = cljs.core.first(arglist__11867);
arglist__11867 = cljs.core.next(arglist__11867);
var id = cljs.core.first(arglist__11867);
var items = cljs.core.rest(arglist__11867);
return selection_list__delegate(label,id,items);
});
selection_list.cljs$core$IFn$_invoke$arity$variadic = selection_list__delegate;
return selection_list;
})()
;
safenote.core.form = new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$98,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$171,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$172,"Reagent Form"], null)], null),safenote.core.text_input(cljs.core.constant$keyword$173,"First name"),safenote.core.text_input(cljs.core.constant$keyword$174,"Last name"),safenote.core.selection_list.cljs$core$IFn$_invoke$arity$variadic("Favorite drinks",cljs.core.constant$keyword$175,cljs.core.array_seq([new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$176,"Coffee"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$177,"Beer"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$178,"Crab juice"], null)], 0))], null);
safenote.core.save_doc = (function save_doc(doc){
return (function (){
return ajax.core.POST.cljs$core$IFn$_invoke$arity$variadic([cljs.core.str(context),cljs.core.str("/save")].join(''),cljs.core.array_seq([new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$51,new cljs.core.PersistentArrayMap(null, 1, [cljs.core.constant$keyword$154,(function (){var G__11869 = doc;
return (cljs.core.deref.cljs$core$IFn$_invoke$arity$1 ? cljs.core.deref.cljs$core$IFn$_invoke$arity$1(G__11869) : cljs.core.deref.call(null,G__11869));
})()], null),cljs.core.constant$keyword$55,(function (_){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(safenote.core.state,cljs.core.assoc,cljs.core.constant$keyword$164,true);
})], null)], 0));
});
});
safenote.core.about = (function about(){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$98,"this is the story of safenote... work in progress"], null);
});
safenote.core.home = (function home(){
var doc = reagent.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
return ((function (doc){
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$98,new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [reagent_forms.core.bind_fields,safenote.core.form,doc,((function (doc){
return (function (_,___$1,___$2){
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(safenote.core.state,cljs.core.assoc,cljs.core.constant$keyword$164,false);

return null;
});})(doc))
], null),(cljs.core.truth_(cljs.core.constant$keyword$164.cljs$core$IFn$_invoke$arity$1((function (){var G__11871 = safenote.core.state;
return (cljs.core.deref.cljs$core$IFn$_invoke$arity$1 ? cljs.core.deref.cljs$core$IFn$_invoke$arity$1(G__11871) : cljs.core.deref.call(null,G__11871));
})()))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$179,"Saved"], null):new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$180,new cljs.core.PersistentArrayMap(null, 3, [cljs.core.constant$keyword$34,"submit",cljs.core.constant$keyword$61,"btn btn-default",cljs.core.constant$keyword$181,safenote.core.save_doc(doc)], null),"Submit"], null))], null);
});
;})(doc))
});
safenote.core.navbar = (function navbar(){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$182,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$183,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$184,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$185,new cljs.core.PersistentArrayMap(null, 1, [cljs.core.constant$keyword$186,"#/"], null),"safenote"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$187,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$188,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$150,new cljs.core.PersistentArrayMap(null, 1, [cljs.core.constant$keyword$61,((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(safenote.core.home,cljs.core.constant$keyword$189.cljs$core$IFn$_invoke$arity$1((function (){var G__11874 = safenote.core.state;
return (cljs.core.deref.cljs$core$IFn$_invoke$arity$1 ? cljs.core.deref.cljs$core$IFn$_invoke$arity$1(G__11874) : cljs.core.deref.call(null,G__11874));
})())))?"active":null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$190,new cljs.core.PersistentArrayMap(null, 1, [cljs.core.constant$keyword$96,(function (){
return secretary.core.dispatch_BANG_("#/");
})], null),"Home"], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$150,new cljs.core.PersistentArrayMap(null, 1, [cljs.core.constant$keyword$61,((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(safenote.core.about,cljs.core.constant$keyword$189.cljs$core$IFn$_invoke$arity$1((function (){var G__11875 = safenote.core.state;
return (cljs.core.deref.cljs$core$IFn$_invoke$arity$1 ? cljs.core.deref.cljs$core$IFn$_invoke$arity$1(G__11875) : cljs.core.deref.call(null,G__11875));
})())))?"active":null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$190,new cljs.core.PersistentArrayMap(null, 1, [cljs.core.constant$keyword$96,(function (){
return secretary.core.dispatch_BANG_("#/about");
})], null),"About"], null)], null)], null)], null)], null)], null);
});
safenote.core.page = (function page(){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$189.cljs$core$IFn$_invoke$arity$1((function (){var G__11877 = safenote.core.state;
return (cljs.core.deref.cljs$core$IFn$_invoke$arity$1 ? cljs.core.deref.cljs$core$IFn$_invoke$arity$1(G__11877) : cljs.core.deref.call(null,G__11877));
})())], null);
});
secretary.core.set_config_BANG_(cljs.core.constant$keyword$39,"#");
var action__11837__auto___11880 = (function (params__11838__auto__){
if(cljs.core.map_QMARK_(params__11838__auto__)){
var map__11878 = params__11838__auto__;
var map__11878__$1 = ((cljs.core.seq_QMARK_(map__11878))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__11878):map__11878);
console.log("hi!");

return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(safenote.core.state,cljs.core.assoc,cljs.core.constant$keyword$189,safenote.core.home);
} else {
if(cljs.core.vector_QMARK_(params__11838__auto__)){
var vec__11879 = params__11838__auto__;
console.log("hi!");

return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(safenote.core.state,cljs.core.assoc,cljs.core.constant$keyword$189,safenote.core.home);
} else {
return null;
}
}
});
secretary.core.add_route_BANG_("/",action__11837__auto___11880);

var action__11837__auto___11883 = (function (params__11838__auto__){
if(cljs.core.map_QMARK_(params__11838__auto__)){
var map__11881 = params__11838__auto__;
var map__11881__$1 = ((cljs.core.seq_QMARK_(map__11881))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__11881):map__11881);
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(safenote.core.state,cljs.core.assoc,cljs.core.constant$keyword$189,safenote.core.about);
} else {
if(cljs.core.vector_QMARK_(params__11838__auto__)){
var vec__11882 = params__11838__auto__;
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(safenote.core.state,cljs.core.assoc,cljs.core.constant$keyword$189,safenote.core.about);
} else {
return null;
}
}
});
secretary.core.add_route_BANG_("/about",action__11837__auto___11883);

safenote.core.init_BANG_ = (function init_BANG_(){
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(safenote.core.state,cljs.core.assoc,cljs.core.constant$keyword$189,safenote.core.home);

reagent.core.render_component.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [safenote.core.navbar], null),document.getElementById("navbar"));

return reagent.core.render_component.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [safenote.core.page], null),document.getElementById("app"));
});
